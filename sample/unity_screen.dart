import 'package:flutter/material.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';

class UnityScreen extends StatefulWidget {
  const UnityScreen({super.key, required this.token});
  final String token;
  @override
  State<UnityScreen> createState() => _UnityScreenState();
}

class _UnityScreenState extends State<UnityScreen> {
  UnityWidgetController? unityWidgetController;
  bool unityLoaded = false;

  @override
  void initState() {
    super.initState();
    unityLoaded = false;
  }

  @override
  void dispose() {
    unityWidgetController?.dispose();
    unityLoaded = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint('token: ${widget.token}');
    return Scaffold(
      body: Stack(
        children: [
          UnityWidget(
            onUnityCreated: onUnityCreated,
            onUnityMessage: onUnityMessage,
            onUnitySceneLoaded: onUnitySceneLoaded,
            useAndroidViewSurface: true,
            fullscreen: true,
          ),
          Visibility(
            visible: !unityLoaded,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  'assets/images/game30y_loading.webp',
                  fit: BoxFit.fill,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void onMessageToUnity(String message) {
    // unityWidgetController?.postMessage(gameObject, methodName, message);
  }

  void onUnityMessage(message) {
    debugPrint('Received message from Unity: ${message.toString()}');
    if (message.toString().toLowerCase().contains('unityloaded')) {
      setState(() {
        unityLoaded = true;
      });
    } else if (message.toString().toLowerCase().contains('quit')) {
      Navigator.pop(context);
    } else if (message.toString().toLowerCase().contains('linkid')) {
      debugPrint('Navigate to linkid');
    }
  }

  void onUnitySceneLoaded(SceneLoaded? scene) {
    debugPrint('onUnitySceneLoaded ${scene?.name}');
    if (scene != null) {
      debugPrint('Received scene loaded from unity: ${scene.name}');
      debugPrint('Received scene loaded from unity: ${scene.buildIndex}');
      debugPrint('Flutter postMessage to unity: ${widget.token}');
    } else {
      debugPrint('Received scene loaded from unity: null');
    }
  }

  void onUnityCreated(controller) {
    controller.resume();
    unityWidgetController = controller;
    debugPrint('onUnityCreated');
    unityWidgetController?.postMessage('Canvas', 'GetToken', widget.token);
  }
}
