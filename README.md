<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->

### Getting started

Install flutter_unity_widget
https://pub.dev/packages/flutter_unity_widget

Clone game30_linkid
https://gitlab.com/quanghuy.le90/game-30y-linkid.git

Install Android NDK

### Setup

## Android
1. Open the android/settings.gradle file and change the following:
```
+    include ":unityLibrary"
+    project(":unityLibrary").projectDir = file("../../game-30y-linkid/android/unityLibrary")
```

2. Open the android/app/build.gradle file and change the following:
```
dependencies {
+        implementation project(':unityLibrary')
     }
```

3.  If you need to build a release package, open the android/app/build.gradle file and change the following:
```
buildTypes {
         release {
             signingConfig signingConfigs.debug
         }
+        debug {
+            signingConfig signingConfigs.debug
+        }
+        profile {
+            signingConfig signingConfigs.debug
+        }
+        innerTest {
+            matchingFallbacks = ['debug', 'release']
+        }
   }
```

4. Set minSdkVersion, open the android/app/build.gradle file and change the following:
```
defaultConfig {
        // minSdkVersion flutter.minSdkVersion
+       minSdkVersion 22
        targetSdkVersion flutter.targetSdkVersion
        versionCode flutterVersionCode.toInteger()
        versionName flutterVersionName
    }
```

5. If you use minifyEnabled true in your android/unityLibrary/build.gradle file, open the android/unityLibrary/proguard-unity.txt and change the following:
```
+    -keep class com.xraph.plugin.** {*;}
```

6. Change NDK path. open the android/app/build.gradle file and change the following:
```
//  commandLineArgs.add("--tool-chain-path=" + android.ndkDirectory)
    commandLineArgs.add("--tool-chain-path=" + "NDK path")
```

## iOS
1. Open the ios/Runner.xcworkspace (workspace, not the project) file in Xcode, right-click on the Navigator (not on an item), go to Add Files to "Runner" and add the game-30y-linkid/ios/UnityLibrary/Unity-Iphone.xcodeproj file.

2. Select the Unity-iPhone/Data folder and change the Target Membership for Data folder to UnityFramework.

3. Set minTargetDeploy 12

4a.  If you're using Swift, open the ios/Runner/AppDelegate.swift file and change the following:
```
     import UIKit
     import Flutter
+    import flutter_unity_widget

     @UIApplicationMain
     @objc class AppDelegate: FlutterAppDelegate {
         override func application(
             _ application: UIApplication,
             didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
         ) -> Bool {
+            InitUnityIntegrationWithOptions(argc: CommandLine.argc, argv: CommandLine.unsafeArgv, launchOptions)

             GeneratedPluginRegistrant.register(with: self)
             return super.application(application, didFinishLaunchingWithOptions: launchOptions)
         }
     }
```

4b. If you're using Objective-C, open the ios/Runner/main.m file and change the following:
```
+    #import "flutter_unity_widget.swift.h"

     int main(int argc, char * argv[]) {
          @autoreleasepool {
+             InitUnityIntegration(argc, argv);
              return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
          }
     }
```

5. Open the ios/Runner/Info.plist and change the following:
```
     <dict>
+        <key>io.flutter.embedded_views_preview</key>
+        <string>YES</string>
     </dict>
```

6. Add the UnityFramework.framework file as a library to the Runner project.

### Additional information

## Android
Remove these lines
```
commandLineArgs.add("--enable-debugger") 
commandLineArgs.add("--profiler-report") 
commandLineArgs.add("--profiler-output-file=" + workingDir + "/build/il2cpp_"+ abi + "" + configuration + "/il2cpp_conv.traceevents") 
commandLineArgs.add("--print-command-line")
```

## iOS
Classes/UnityAppController.mm

```
- (void)application:(UIApplication*)application handleEventsForBackgroundURLSession:(nonnull NSString ...

+
extern "C" void OnUnityMessage(const char* message)
{
    if (GetAppController().unityMessageHandler) {
        GetAppController().unityMessageHandler(message);
    }
}

extern "C" void OnUnitySceneLoaded(const char* name, const int* buildIndex, const bool* isLoaded, const bool* IsValid)
{
    if (GetAppController().unitySceneLoadedHandler) {
        GetAppController().unitySceneLoadedHandler(name, buildIndex, isLoaded, IsValid);
    }
}
```

Classes/UnityAppController.h
```
@property (nonatomic, copy)                                 void (^quitHandler)(void);...
+
@property (nonatomic, copy)                                 void(^unitySceneLoadedHandler)(const char* name, const int* buildIndex, const bool* isLoaded, const bool* IsValid);
@property (nonatomic, copy)                                 void(^unityMessageHandler)(const char* message);
```

## Others
Conflict webview_flutter
Edit pubspec.yaml
```
dependencies:
  .....
dependency_overrides:
  webview_flutter: lastest_version
```

## Usage

Send message to Unity

```
  UnityWidgetController? unityWidgetController;
  unityWidgetController?.postMessage(gameObject, methodName, message);
```

Receive message from Unity

```
  void onUnityMessage(message) {
    debugPrint('Received message from Unity: ${message.toString()}');
  }
```

iOS build flavors
```
https://github.com/juicycleff/flutter-unity-view-widget#flavors
```


