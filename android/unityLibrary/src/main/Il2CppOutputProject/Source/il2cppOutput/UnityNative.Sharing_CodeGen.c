﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





extern void AndroidUnityNativeSharingAdapter__ctor_m02105AE1B7A9F5A637D6D49F28E8BDB56700546F (void);
extern void AndroidUnityNativeSharingAdapter_ShareScreenshotAndText_m7861490C654A66C3AFC1133D57D98D6127F16C93 (void);
extern void AndroidUnityNativeSharingAdapter_ShareText_m89EF46D182F10FB6D0BF41A31CCC92424318D6ED (void);
extern void UnityNativeSharing_Create_m659A1E5822976EFACAC96EA3C864748ACE462E13 (void);
extern void UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD (void);
extern void UnityNativeSharing_ShareScreenshotAndText_m4D796344268E8048062152322841F90EFBE213CD (void);
extern void UnityNativeSharing_ShareText_m6BE537F2B56982778AC3B7ECA28CA488D64341A9 (void);
static Il2CppMethodPointer s_methodPointers[11] = 
{
	AndroidUnityNativeSharingAdapter__ctor_m02105AE1B7A9F5A637D6D49F28E8BDB56700546F,
	AndroidUnityNativeSharingAdapter_ShareScreenshotAndText_m7861490C654A66C3AFC1133D57D98D6127F16C93,
	AndroidUnityNativeSharingAdapter_ShareText_m89EF46D182F10FB6D0BF41A31CCC92424318D6ED,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityNativeSharing_Create_m659A1E5822976EFACAC96EA3C864748ACE462E13,
	UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD,
	UnityNativeSharing_ShareScreenshotAndText_m4D796344268E8048062152322841F90EFBE213CD,
	UnityNativeSharing_ShareText_m6BE537F2B56982778AC3B7ECA28CA488D64341A9,
};
static const int32_t s_InvokerIndices[11] = 
{
	2303,
	414,
	1014,
	0,
	0,
	0,
	0,
	4667,
	2063,
	414,
	1014,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityNative_Sharing_CodeGenModule;
const Il2CppCodeGenModule g_UnityNative_Sharing_CodeGenModule = 
{
	"UnityNative.Sharing.dll",
	11,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
