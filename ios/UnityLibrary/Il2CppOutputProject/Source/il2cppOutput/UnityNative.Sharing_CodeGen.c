﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





extern void IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareTextAndScreenshot_mAD7E41145E1F5687F9B6ED788D9A1F166B1A0763 (void);
extern void IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareText_m48E027E0801C31B683596AEF30EB4F8E5CE8C5D2 (void);
extern void IosUnityNativeSharingAdapter_ShareScreenshotAndText_m689A7A38ADE318AF82C42FCAA7AEBEF92C89BBD1 (void);
extern void IosUnityNativeSharingAdapter_ShareText_mA940BAB90566E2526E05E1DF1EBEAD51889505E5 (void);
extern void IosUnityNativeSharingAdapter__ctor_m941379B5B49FDB2D8778B978234F62F4A8FEC786 (void);
extern void UnityNativeSharing_Create_m659A1E5822976EFACAC96EA3C864748ACE462E13 (void);
extern void UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD (void);
extern void UnityNativeSharing_ShareScreenshotAndText_m4D796344268E8048062152322841F90EFBE213CD (void);
extern void UnityNativeSharing_ShareText_m6BE537F2B56982778AC3B7ECA28CA488D64341A9 (void);
static Il2CppMethodPointer s_methodPointers[13] = 
{
	IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareTextAndScreenshot_mAD7E41145E1F5687F9B6ED788D9A1F166B1A0763,
	IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareText_m48E027E0801C31B683596AEF30EB4F8E5CE8C5D2,
	IosUnityNativeSharingAdapter_ShareScreenshotAndText_m689A7A38ADE318AF82C42FCAA7AEBEF92C89BBD1,
	IosUnityNativeSharingAdapter_ShareText_mA940BAB90566E2526E05E1DF1EBEAD51889505E5,
	IosUnityNativeSharingAdapter__ctor_m941379B5B49FDB2D8778B978234F62F4A8FEC786,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityNativeSharing_Create_m659A1E5822976EFACAC96EA3C864748ACE462E13,
	UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD,
	UnityNativeSharing_ShareScreenshotAndText_m4D796344268E8048062152322841F90EFBE213CD,
	UnityNativeSharing_ShareText_m6BE537F2B56982778AC3B7ECA28CA488D64341A9,
};
static const int32_t s_InvokerIndices[13] = 
{
	4070,
	4607,
	421,
	1032,
	2327,
	0,
	0,
	0,
	0,
	4679,
	2085,
	421,
	1032,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityNative_Sharing_CodeGenModule;
const Il2CppCodeGenModule g_UnityNative_Sharing_CodeGenModule = 
{
	"UnityNative.Sharing.dll",
	13,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
