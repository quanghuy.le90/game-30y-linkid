﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif





struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
struct Dictionary_2_t41165BF747F041590086BE39A59BE164430A3CEF;
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
struct IEnumerable_1_tDFE91FA569BD0F8AF243E1AB885C63ABE1F96024;
struct List_1_tECB13E82883EA864AEBA60A256302E1C8CFD6EF4;
struct List_1_t70EE7982F45810D4B024CF720D910E67974A3094;
struct Task_1_t4C228DE57804012969575431CFF12D57C875552D;
struct AnimationCurveU5BU5D_t2C4A38D7EFA8095F32316A4D9CE4CBB6840FB7EC;
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct ConfigurationEntryU5BU5D_t52C052ED69E59F867806855793A30109BBA9EEF3;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct PlayableBindingU5BU5D_tC50C3F27A8E4246488F7A5998CAABAC4811A92CD;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
struct AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354;
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6;
struct FontAsset_t61A6446D934E582651044E33D250EA8D306AB958;
struct Hashtable_tEFC3B6496E6747787D8BB761B51F2AE3A8CFFE2D;
struct IPackageRegistry_t598A337CA30E50021BB139EF5CF3C3867139CFA2;
struct IPinnable_tA3989EA495C0118966BAAF8848C0009947BB49C0;
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
struct MemberInfo_t;
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
struct String_t;
struct StyleComplexSelector_tE46C29F65FDBA48D3152781187401C8B55B7D8AD;
struct StyleSheet_t6FAF43FCDB45BC6BED0522A222FD4C1A9BB10428;
struct StyleValueCollection_t5ADC08D23E648FBE78F2C161494786E6C83E1377;
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
struct Type_t;
struct VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC;
struct VisualElement_t2667F9D19E62C7A315927506C06F223AB9234115;
struct VisualTreeAsset_tFB5BF81F0780A412AE5A7C2C552B3EEA64EA2EEB;
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
struct CreateOutputMethod_tD18AFE3B69E6DDD913D82D5FA1D5D909CEEC8509;
struct unitytls_tlsctx_read_callback_tDBE877327789CABE940C2A724EC9A5D142318851;
struct unitytls_tlsctx_write_callback_t5D4B64AD846D04E819A49689F7EAA47365636611;

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};
struct ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____array;
	int32_t ____offset;
	int32_t ____count;
};
struct CustomStyleProperty_1_tE4B20CAB5BCFEE711EB4A26F077DC700987C0C2D 
{
	String_t* ___U3CnameU3Ek__BackingField;
};
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField;
};
#endif
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField;
};
#endif
struct CustomStyleProperty_1_t6871E5DBF19AB4DC7E1134B32A03B7A458D52E9F 
{
	String_t* ___U3CnameU3Ek__BackingField;
};
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField;
};
#endif
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField;
};
#endif
struct CustomStyleProperty_1_t697913D630F101B4E464B7E9EA06368015C9C266 
{
	String_t* ___U3CnameU3Ek__BackingField;
};
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField;
};
#endif
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField;
};
#endif
struct CustomStyleProperty_1_t21332918528099194FD36C74FF0FA14696F39493 
{
	String_t* ___U3CnameU3Ek__BackingField;
};
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField;
};
#endif
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField;
};
#endif
struct JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5 
{
	RuntimeObject* ____enumerable;
};
struct KeyValuePair_2_tDC26B09C26BA829DDE331BCB6AF7C508C763D7A3 
{
	int32_t ___key;
	RuntimeObject* ___value;
};
struct KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 
{
	RuntimeObject* ___key;
	RuntimeObject* ___value;
};
struct Memory_1_tB7CEF4416F5014E364267478CEF016A4AC5C0036 
{
	RuntimeObject* ____object;
	int32_t ____index;
	int32_t ____length;
};
#ifndef Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_pinvoke_define
#define Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_pinvoke_define
struct Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_pinvoke
{
	Il2CppIUnknown* ____object;
	int32_t ____index;
	int32_t ____length;
};
#endif
#ifndef Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_com_define
#define Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_com_define
struct Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_com
{
	Il2CppIUnknown* ____object;
	int32_t ____index;
	int32_t ____length;
};
#endif
struct NativeArray_1_tBEE3484B4ABC271CFAB65039F1439061D5DF806A 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct NativeArray_1_t73992261AA60020B6BE20D83C50B3F925CC89F31 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct NativeArray_1_t4020B6981295FB915DCE82EF368535F680C13A49 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 
{
	bool ___hasValue;
	bool ___value;
};
struct Nullable_1_tEB6689CC9747A3600689077DCBF77B8E8B510505 
{
	bool ___hasValue;
	uint8_t ___value;
};
struct Nullable_1_tD52F1D0FC7EBB336F119BE953E59F426766032C1 
{
	bool ___hasValue;
	Il2CppChar ___value;
};
struct Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 
{
	bool ___hasValue;
	double ___value;
};
struct Nullable_1_t57D99A484501B89DA27E67D6D9A89722D5A7DE2C 
{
	bool ___hasValue;
	int16_t ___value;
};
struct Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 
{
	bool ___hasValue;
	int32_t ___value;
};
struct Nullable_1_t365991B3904FDA7642A788423B28692FDC7CDB17 
{
	bool ___hasValue;
	int64_t ___value;
};
struct Nullable_1_tCF16C2638810B89EAA3EEFE6B35FC71B6AE96B2C 
{
	bool ___hasValue;
	int8_t ___value;
};
struct Nullable_1_t3D746CBB6123D4569FF4DEA60BC4240F32C6FE75 
{
	bool ___hasValue;
	float ___value;
};
struct Nullable_1_t70F850DEE49B62D1B877D3C32F9E0EC724ADC4D9 
{
	bool ___hasValue;
	uint16_t ___value;
};
struct Nullable_1_tD043F01310E483091D0E9A5526C3425F13EF2099 
{
	bool ___hasValue;
	uint32_t ___value;
};
struct Nullable_1_tF8BFF19FF240C9F0A45168187CD7106BAA146A99 
{
	bool ___hasValue;
	uint64_t ___value;
};
struct ReadOnly_t1D4689336F49F434532D72398BFBE7BF4D6059D4 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
};
struct ReadOnlyMemory_1_t63F301BF893B0AB689953D86A641168CA66D2399 
{
	RuntimeObject* ____object;
	int32_t ____index;
	int32_t ____length;
};
#ifndef ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_pinvoke_define
#define ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_pinvoke_define
struct ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_pinvoke
{
	Il2CppIUnknown* ____object;
	int32_t ____index;
	int32_t ____length;
};
#endif
#ifndef ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_com_define
#define ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_com_define
struct ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_com
{
	Il2CppIUnknown* ____object;
	int32_t ____index;
	int32_t ____length;
};
#endif
struct StyleEnum_1_t3DD2EBD4E359AFE77C2974ECAA1DEE50E0FACEDC 
{
	int32_t ___m_Value;
	int32_t ___m_Keyword;
};
struct ValueTask_1_t823DE87C36EA952D24C4E64F532E9D4425B72F21 
{
	RuntimeObject* ____obj;
	int32_t ____result;
	int16_t ____token;
	bool ____continueOnCapturedContext;
};
struct ValueTuple_2_tB358DB210B9947851BE1C2586AD7532BEB639942 
{
	int32_t ___Item1;
	bool ___Item2;
};
struct Angle_t0229F612898D65B3CC646C40A32D93D8A33C1DFC 
{
	float ___m_Value;
	int32_t ___m_Unit;
};
struct Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___m_RenderTexture;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___m_VectorImage;
};
struct Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8_marshaled_pinvoke
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___m_RenderTexture;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___m_VectorImage;
};
struct Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8_marshaled_com
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___m_RenderTexture;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___m_VectorImage;
};
struct BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F 
{
	int32_t ___x;
	int32_t ___y;
};
struct BatchPackedCullingViewID_t1E7EE8631C02555CAA181FA566CDC604B9FEFEBB 
{
	uint64_t ___handle;
};
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED 
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source;
};
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_pinvoke
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source;
};
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_com
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source;
};
struct CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B 
{
	int32_t ___m_Style;
	float ___m_Time;
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___m_CustomCurve;
};
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	float ___r;
	float ___g;
	float ___b;
	float ___a;
};
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___rgba;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___r;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_OffsetPadding[1];
			uint8_t ___g;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_OffsetPadding[2];
			uint8_t ___b;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_OffsetPadding[3];
			uint8_t ___a;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_forAlignmentOnly;
		};
	};
};
struct ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900 
{
	Il2CppChar ____keyChar;
	int32_t ____key;
	int32_t ____mods;
};
struct ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900_marshaled_pinvoke
{
	uint8_t ____keyChar;
	int32_t ____key;
	int32_t ____mods;
};
struct ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900_marshaled_com
{
	uint8_t ____keyChar;
	int32_t ____key;
	int32_t ____mods;
};
struct CoreRegistration_tD2BD53556CAA48BD5E0D32CB92C6494C0EB85581 
{
	RuntimeObject* ___m_Registry;
	int32_t ___m_PackageHash;
};
struct CoreRegistration_tD2BD53556CAA48BD5E0D32CB92C6494C0EB85581_marshaled_pinvoke
{
	RuntimeObject* ___m_Registry;
	int32_t ___m_PackageHash;
};
struct CoreRegistration_tD2BD53556CAA48BD5E0D32CB92C6494C0EB85581_marshaled_com
{
	RuntimeObject* ___m_Registry;
	int32_t ___m_PackageHash;
};
struct CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 
{
	VisualElement_t2667F9D19E62C7A315927506C06F223AB9234115* ___U3CtargetU3Ek__BackingField;
	VisualTreeAsset_tFB5BF81F0780A412AE5A7C2C552B3EEA64EA2EEB* ___U3CvisualTreeAssetU3Ek__BackingField;
	Dictionary_2_t41165BF747F041590086BE39A59BE164430A3CEF* ___U3CslotInsertionPointsU3Ek__BackingField;
	List_1_t70EE7982F45810D4B024CF720D910E67974A3094* ___U3CattributeOverridesU3Ek__BackingField;
};
struct CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257_marshaled_pinvoke
{
	VisualElement_t2667F9D19E62C7A315927506C06F223AB9234115* ___U3CtargetU3Ek__BackingField;
	VisualTreeAsset_tFB5BF81F0780A412AE5A7C2C552B3EEA64EA2EEB* ___U3CvisualTreeAssetU3Ek__BackingField;
	Dictionary_2_t41165BF747F041590086BE39A59BE164430A3CEF* ___U3CslotInsertionPointsU3Ek__BackingField;
	List_1_t70EE7982F45810D4B024CF720D910E67974A3094* ___U3CattributeOverridesU3Ek__BackingField;
};
struct CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257_marshaled_com
{
	VisualElement_t2667F9D19E62C7A315927506C06F223AB9234115* ___U3CtargetU3Ek__BackingField;
	VisualTreeAsset_tFB5BF81F0780A412AE5A7C2C552B3EEA64EA2EEB* ___U3CvisualTreeAssetU3Ek__BackingField;
	Dictionary_2_t41165BF747F041590086BE39A59BE164430A3CEF* ___U3CslotInsertionPointsU3Ek__BackingField;
	List_1_t70EE7982F45810D4B024CF720D910E67974A3094* ___U3CattributeOverridesU3Ek__BackingField;
};
struct CullingGroupEvent_tC79BA328A8280C29F6002F591614081A0E87D110 
{
	int32_t ___m_Index;
	uint8_t ___m_PrevState;
	uint8_t ___m_ThisState;
};
struct CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F 
{
	Type_t* ___U3CArgumentTypeU3Ek__BackingField;
	RuntimeObject* ___U3CValueU3Ek__BackingField;
};
struct CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F_marshaled_pinvoke
{
	Type_t* ___U3CArgumentTypeU3Ek__BackingField;
	Il2CppIUnknown* ___U3CValueU3Ek__BackingField;
};
struct CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F_marshaled_com
{
	Type_t* ___U3CArgumentTypeU3Ek__BackingField;
	Il2CppIUnknown* ___U3CValueU3Ek__BackingField;
};
struct DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___P;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Q;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___G;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Y;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___J;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___X;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Seed;
	int32_t ___Counter;
};
struct DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9_marshaled_pinvoke
{
	Il2CppSafeArray* ___P;
	Il2CppSafeArray* ___Q;
	Il2CppSafeArray* ___G;
	Il2CppSafeArray* ___Y;
	Il2CppSafeArray* ___J;
	Il2CppSafeArray* ___X;
	Il2CppSafeArray* ___Seed;
	int32_t ___Counter;
};
struct DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9_marshaled_com
{
	Il2CppSafeArray* ___P;
	Il2CppSafeArray* ___Q;
	Il2CppSafeArray* ___G;
	Il2CppSafeArray* ___Y;
	Il2CppSafeArray* ___J;
	Il2CppSafeArray* ___X;
	Il2CppSafeArray* ___Seed;
	int32_t ___Counter;
};
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D 
{
	uint64_t ____dateData;
};
struct Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___flags;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___flags_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___hi_OffsetPadding[4];
			int32_t ___hi;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___hi_OffsetPadding_forAlignmentOnly[4];
			int32_t ___hi_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___lo_OffsetPadding[8];
			int32_t ___lo;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___lo_OffsetPadding_forAlignmentOnly[8];
			int32_t ___lo_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___mid_OffsetPadding[12];
			int32_t ___mid;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___mid_OffsetPadding_forAlignmentOnly[12];
			int32_t ___mid_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___ulomidLE_OffsetPadding[8];
			uint64_t ___ulomidLE;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___ulomidLE_OffsetPadding_forAlignmentOnly[8];
			uint64_t ___ulomidLE_forAlignmentOnly;
		};
	};
};
struct DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB 
{
	RuntimeObject* ____key;
	RuntimeObject* ____value;
};
struct DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB_marshaled_pinvoke
{
	Il2CppIUnknown* ____key;
	Il2CppIUnknown* ____value;
};
struct DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB_marshaled_com
{
	Il2CppIUnknown* ____key;
	Il2CppIUnknown* ____value;
};
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 
{
	bool ___U3CwantsMouseMoveU3Ek__BackingField;
	bool ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField;
	bool ___U3CwantsLessLayoutEventsU3Ek__BackingField;
};
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_pinvoke
{
	int32_t ___U3CwantsMouseMoveU3Ek__BackingField;
	int32_t ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField;
	int32_t ___U3CwantsLessLayoutEventsU3Ek__BackingField;
};
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_com
{
	int32_t ___U3CwantsMouseMoveU3Ek__BackingField;
	int32_t ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField;
	int32_t ___U3CwantsLessLayoutEventsU3Ek__BackingField;
};
struct FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C 
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___m_FontAsset;
};
struct FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___m_FontAsset;
};
struct FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___m_FontAsset;
};
struct Guid_t 
{
	int32_t ____a;
	int16_t ____b;
	int16_t ____c;
	uint8_t ____d;
	uint8_t ____e;
	uint8_t ____f;
	uint8_t ____g;
	uint8_t ____h;
	uint8_t ____i;
	uint8_t ____j;
	uint8_t ____k;
};
struct Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 
{
	uint64_t ___u64_0;
	uint64_t ___u64_1;
};
struct IntPtr_t 
{
	void* ___m_value;
};
struct JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 
{
	uint64_t ___jobGroup;
	int32_t ___version;
};
struct Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 
{
	float ___m_Value;
	int32_t ___m_Unit;
};
struct LineInfo_t415DCF0EAD0FB3806F779BA170EC9058E47CCB24 
{
	int32_t ___lineNo;
	int32_t ___linePos;
};
struct LoadSceneParameters_tFBAFEA7FA75F282D3034241AD8756A7B5578310E 
{
	int32_t ___m_LoadSceneMode;
	int32_t ___m_LocalPhysicsMode;
};
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	float ___m00;
	float ___m10;
	float ___m20;
	float ___m30;
	float ___m01;
	float ___m11;
	float ___m21;
	float ___m31;
	float ___m02;
	float ___m12;
	float ___m22;
	float ___m32;
	float ___m03;
	float ___m13;
	float ___m23;
	float ___m33;
};
struct PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE 
{
	int32_t ___m_Handle;
};
struct PropertyName_tE4B4AAA58AF3BF2C0CD95509EB7B786F096901C2 
{
	int32_t ___id;
};
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Exponent;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Modulus;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___P;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Q;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___DP;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___DQ;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___InverseQ;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___D;
};
struct RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF_marshaled_pinvoke
{
	Il2CppSafeArray* ___Exponent;
	Il2CppSafeArray* ___Modulus;
	Il2CppSafeArray* ___P;
	Il2CppSafeArray* ___Q;
	Il2CppSafeArray* ___DP;
	Il2CppSafeArray* ___DQ;
	Il2CppSafeArray* ___InverseQ;
	Il2CppSafeArray* ___D;
};
struct RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF_marshaled_com
{
	Il2CppSafeArray* ___Exponent;
	Il2CppSafeArray* ___Modulus;
	Il2CppSafeArray* ___P;
	Il2CppSafeArray* ___Q;
	Il2CppSafeArray* ___DP;
	Il2CppSafeArray* ___DQ;
	Il2CppSafeArray* ___InverseQ;
	Il2CppSafeArray* ___D;
};
struct Range_tDDBAD7CBDC5DD273DA4330F4E9CDF24C62F16ED6 
{
	int32_t ___from;
	int32_t ___count;
};
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	float ___m_XMin;
	float ___m_YMin;
	float ___m_Width;
	float ___m_Height;
};
struct RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 
{
	int32_t ___m_XMin;
	int32_t ___m_YMin;
	int32_t ___m_Width;
	int32_t ___m_Height;
};
struct RuleMatcher_t327CFEB02C81AA20E639DE949DCBBAB5E92FF28E 
{
	StyleSheet_t6FAF43FCDB45BC6BED0522A222FD4C1A9BB10428* ___sheet;
	StyleComplexSelector_tE46C29F65FDBA48D3152781187401C8B55B7D8AD* ___complexSelector;
};
struct RuleMatcher_t327CFEB02C81AA20E639DE949DCBBAB5E92FF28E_marshaled_pinvoke
{
	StyleSheet_t6FAF43FCDB45BC6BED0522A222FD4C1A9BB10428* ___sheet;
	StyleComplexSelector_tE46C29F65FDBA48D3152781187401C8B55B7D8AD* ___complexSelector;
};
struct RuleMatcher_t327CFEB02C81AA20E639DE949DCBBAB5E92FF28E_marshaled_com
{
	StyleSheet_t6FAF43FCDB45BC6BED0522A222FD4C1A9BB10428* ___sheet;
	StyleComplexSelector_tE46C29F65FDBA48D3152781187401C8B55B7D8AD* ___complexSelector;
};
struct SerializableProjectConfiguration_tBAE4D3A66EC38C1869E294396DB79F127B8F58EE 
{
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___Keys;
	ConfigurationEntryU5BU5D_t52C052ED69E59F867806855793A30109BBA9EEF3* ___Values;
};
struct SerializableProjectConfiguration_tBAE4D3A66EC38C1869E294396DB79F127B8F58EE_marshaled_pinvoke
{
	char** ___Keys;
	ConfigurationEntryU5BU5D_t52C052ED69E59F867806855793A30109BBA9EEF3* ___Values;
};
struct SerializableProjectConfiguration_tBAE4D3A66EC38C1869E294396DB79F127B8F58EE_marshaled_com
{
	Il2CppChar** ___Keys;
	ConfigurationEntryU5BU5D_t52C052ED69E59F867806855793A30109BBA9EEF3* ___Values;
};
struct ShaderTagId_t453E2085B5EE9448FF75E550CAB111EFF690ECB0 
{
	int32_t ___m_Id;
};
struct StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 
{
	String_t* ___U3CtitleU3Ek__BackingField;
	int32_t ___U3CvisualModeU3Ek__BackingField;
	Hashtable_tEFC3B6496E6747787D8BB761B51F2AE3A8CFFE2D* ___U3CgenericDataU3Ek__BackingField;
	RuntimeObject* ___U3CunityObjectReferencesU3Ek__BackingField;
};
struct StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9_marshaled_pinvoke
{
	char* ___U3CtitleU3Ek__BackingField;
	int32_t ___U3CvisualModeU3Ek__BackingField;
	Hashtable_tEFC3B6496E6747787D8BB761B51F2AE3A8CFFE2D* ___U3CgenericDataU3Ek__BackingField;
	RuntimeObject* ___U3CunityObjectReferencesU3Ek__BackingField;
};
struct StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9_marshaled_com
{
	Il2CppChar* ___U3CtitleU3Ek__BackingField;
	int32_t ___U3CvisualModeU3Ek__BackingField;
	Hashtable_tEFC3B6496E6747787D8BB761B51F2AE3A8CFFE2D* ___U3CgenericDataU3Ek__BackingField;
	RuntimeObject* ___U3CunityObjectReferencesU3Ek__BackingField;
};
struct StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 
{
	RuntimeObject* ___m_additionalContext;
	int32_t ___m_state;
};
struct StreamingContext_t56760522A751890146EE45F82F866B55B7E33677_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext;
	int32_t ___m_state;
};
struct StreamingContext_t56760522A751890146EE45F82F866B55B7E33677_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext;
	int32_t ___m_state;
};
struct StyleFloat_t4A100BCCDC275C2302517C5858C9BE9EC43D4841 
{
	float ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFont_t9D8A6F3E224B60FD8BA1522CE8AB0E2E8BE8B77C 
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFont_t9D8A6F3E224B60FD8BA1522CE8AB0E2E8BE8B77C_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFont_t9D8A6F3E224B60FD8BA1522CE8AB0E2E8BE8B77C_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D 
{
	int32_t ___m_ValueType;
	int32_t ___valueIndex;
};
struct StyleValues_t4AED947A53B84B62EF2B589A40B74911CA77D11A 
{
	StyleValueCollection_t5ADC08D23E648FBE78F2C161494786E6C83E1377* ___m_StyleValues;
};
struct StyleValues_t4AED947A53B84B62EF2B589A40B74911CA77D11A_marshaled_pinvoke
{
	StyleValueCollection_t5ADC08D23E648FBE78F2C161494786E6C83E1377* ___m_StyleValues;
};
struct StyleValues_t4AED947A53B84B62EF2B589A40B74911CA77D11A_marshaled_com
{
	StyleValueCollection_t5ADC08D23E648FBE78F2C161494786E6C83E1377* ___m_StyleValues;
};
struct TextureId_tFF4B4AAE53408AB10B0B89CCA5F7B50CF2535E58 
{
	int32_t ___m_Index;
};
struct TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A 
{
	int64_t ____ticks;
};
struct TimerState_t82C7C29B095D6ACDC06AC172C269E9D5F0508ECE 
{
	int64_t ___U3CstartU3Ek__BackingField;
	int64_t ___U3CnowU3Ek__BackingField;
};
struct UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC 
{
	int32_t ___startCharIdx;
	int32_t ___height;
	float ___topY;
	float ___leading;
};
struct ValueTask_t10B4B5DDF5C582607D0E634FA912F8CB94FCD49F 
{
	RuntimeObject* ____obj;
	int16_t ____token;
	bool ____continueOnCapturedContext;
};
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	float ___x;
	float ___y;
};
struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A 
{
	int32_t ___m_X;
	int32_t ___m_Y;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	float ___x;
	float ___y;
	float ___z;
};
struct Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 
{
	int32_t ___m_X;
	int32_t ___m_Y;
	int32_t ___m_Z;
};
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB 
{
	float ___width;
	float ___height;
};
struct CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB 
{
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___m_Custom;
	float ___m_Weight;
};
struct unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 
{
	uint32_t ___magic;
	uint32_t ___code;
	uint64_t ___reserved;
};
struct unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 
{
	uint64_t ___handle;
};
struct unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 
{
	unitytls_tlsctx_read_callback_tDBE877327789CABE940C2A724EC9A5D142318851* ___read;
	unitytls_tlsctx_write_callback_t5D4B64AD846D04E819A49689F7EAA47365636611* ___write;
	void* ___data;
};
struct unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568_marshaled_pinvoke
{
	Il2CppMethodPointer ___read;
	Il2CppMethodPointer ___write;
	void* ___data;
};
struct unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568_marshaled_com
{
	Il2CppMethodPointer ___read;
	Il2CppMethodPointer ___write;
	void* ___data;
};
struct unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 
{
	uint32_t ___min;
	uint32_t ___max;
};
struct unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 
{
	uint64_t ___handle;
};
struct unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 
{
	uint64_t ___handle;
};
struct ByReference_1_t9C85BCCAAF8C525B6C06B07E922D8D217BE8D6FC 
{
	intptr_t ____value;
};
struct ByReference_1_t7BA5A6CA164F770BC688F21C5978D368716465F5 
{
	intptr_t ____value;
};
struct ByReference_1_t98B79BFB40A2CA0814BC183B09B4339A5EBF8524 
{
	intptr_t ____value;
};
struct Nullable_1_tEADC262F7F8B8BC4CC0A003DBDD3CA7C1B63F9AC 
{
	bool ___hasValue;
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___value;
};
struct Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB 
{
	bool ___hasValue;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___value;
};
struct Nullable_1_t0ECB838EB0C9A81655750B26970F21CF9A83A5F7 
{
	bool ___hasValue;
	Guid_t ___value;
};
struct Nullable_1_tE151CE1F6892804B41C4004C95CB57020ABB3272 
{
	bool ___hasValue;
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___value;
};
struct BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 
{
	int32_t ___keyword;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___offset;
};
struct BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 
{
	int32_t ___m_SizeType;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
};
struct BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F 
{
	NativeArray_1_tBEE3484B4ABC271CFAB65039F1439061D5DF806A ___drawCommands;
};
struct BoundingSphere_t2DDB3D1711A6920C0ECA9217D3E4E14AFF03C010 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position;
	float ___radius;
};
struct Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Center;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Extents;
};
struct BoundsInt_t4E757DE5EFF9FCB42000F173360DDC63B5585485 
{
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___m_Position;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___m_Size;
};
struct ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0 
{
	bool ___isValid;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___pageAndID;
};
struct ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0_marshaled_pinvoke
{
	int32_t ___isValid;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___pageAndID;
};
struct ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0_marshaled_com
{
	int32_t ___isValid;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___pageAndID;
};
struct Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82 
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___U3CtextureU3Ek__BackingField;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3ChotspotU3Ek__BackingField;
	int32_t ___U3CdefaultCursorIdU3Ek__BackingField;
};
struct Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82_marshaled_pinvoke
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___U3CtextureU3Ek__BackingField;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3ChotspotU3Ek__BackingField;
	int32_t ___U3CdefaultCursorIdU3Ek__BackingField;
};
struct Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82_marshaled_com
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___U3CtextureU3Ek__BackingField;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3ChotspotU3Ek__BackingField;
	int32_t ___U3CdefaultCursorIdU3Ek__BackingField;
};
struct CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02 
{
	CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F ___U3CTypedValueU3Ek__BackingField;
	bool ___U3CIsFieldU3Ek__BackingField;
	String_t* ___U3CMemberNameU3Ek__BackingField;
	Type_t* ____attributeType;
	MemberInfo_t* ____lazyMemberInfo;
};
struct CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02_marshaled_pinvoke
{
	CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F_marshaled_pinvoke ___U3CTypedValueU3Ek__BackingField;
	int32_t ___U3CIsFieldU3Ek__BackingField;
	char* ___U3CMemberNameU3Ek__BackingField;
	Type_t* ____attributeType;
	MemberInfo_t* ____lazyMemberInfo;
};
struct CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02_marshaled_com
{
	CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F_marshaled_com ___U3CTypedValueU3Ek__BackingField;
	int32_t ___U3CIsFieldU3Ek__BackingField;
	Il2CppChar* ___U3CMemberNameU3Ek__BackingField;
	Type_t* ____attributeType;
	MemberInfo_t* ____lazyMemberInfo;
};
struct DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 
{
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ____dateTime;
	int16_t ____offsetMinutes;
};
struct GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC 
{
	intptr_t ___handle;
};
struct LODParameters_t54D2AA0FD8E53BCF51D7A42BC1A72FCA8C78A08A 
{
	int32_t ___m_IsOrthographic;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_CameraPosition;
	float ___m_FieldOfView;
	float ___m_OrthoSize;
	int32_t ___m_CameraPixelHeight;
};
struct LensSettings_t6DAB2F204EC22686BF4397E0871B4875414A84FE 
{
	float ___FieldOfView;
	float ___OrthographicSize;
	float ___NearClipPlane;
	float ___FarClipPlane;
	float ___Dutch;
	int32_t ___ModeOverride;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___LensShift;
	int32_t ___GateFit;
	float ___FocusDistance;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_SensorSize;
	bool ___m_OrthoFromCamera;
	bool ___m_PhysicalFromCamera;
};
struct LensSettings_t6DAB2F204EC22686BF4397E0871B4875414A84FE_marshaled_pinvoke
{
	float ___FieldOfView;
	float ___OrthographicSize;
	float ___NearClipPlane;
	float ___FarClipPlane;
	float ___Dutch;
	int32_t ___ModeOverride;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___LensShift;
	int32_t ___GateFit;
	float ___FocusDistance;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_SensorSize;
	int32_t ___m_OrthoFromCamera;
	int32_t ___m_PhysicalFromCamera;
};
struct LensSettings_t6DAB2F204EC22686BF4397E0871B4875414A84FE_marshaled_com
{
	float ___FieldOfView;
	float ___OrthographicSize;
	float ___NearClipPlane;
	float ___FarClipPlane;
	float ___Dutch;
	int32_t ___ModeOverride;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___LensShift;
	int32_t ___GateFit;
	float ___FocusDistance;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_SensorSize;
	int32_t ___m_OrthoFromCamera;
	int32_t ___m_PhysicalFromCamera;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr;
};
struct PenData_t2345B5FBD18D851528C5C18F8A667D4EF4690945 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___position;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___tilt;
	int32_t ___penStatus;
	float ___twist;
	float ___pressure;
	int32_t ___contactType;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___deltaPos;
};
struct PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E 
{
	intptr_t ___m_Handle;
	uint32_t ___m_Version;
};
struct PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 
{
	intptr_t ___m_Handle;
	uint32_t ___m_Version;
};
struct PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 
{
	intptr_t ___m_Handle;
	uint32_t ___m_Version;
};
struct ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD 
{
	intptr_t ___m_Ptr;
};
struct Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Origin;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Direction;
};
struct RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Centroid;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal;
	float ___m_Distance;
	float ___m_Fraction;
	int32_t ___m_Collider;
};
struct Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 
{
	Angle_t0229F612898D65B3CC646C40A32D93D8A33C1DFC ___m_Angle;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Axis;
	bool ___m_IsNone;
};
struct Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7_marshaled_pinvoke
{
	Angle_t0229F612898D65B3CC646C40A32D93D8A33C1DFC ___m_Angle;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Axis;
	int32_t ___m_IsNone;
};
struct Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7_marshaled_com
{
	Angle_t0229F612898D65B3CC646C40A32D93D8A33C1DFC ___m_Angle;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Axis;
	int32_t ___m_IsNone;
};
struct RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 
{
	intptr_t ___value;
};
struct RuntimeMethodHandle_tB35B96E97214DCBE20B0B02B1E687884B34680B2 
{
	intptr_t ___value;
};
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	intptr_t ___value;
};
struct Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Scale;
	bool ___m_IsNone;
};
struct Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7_marshaled_pinvoke
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Scale;
	int32_t ___m_IsNone;
};
struct Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7_marshaled_com
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Scale;
	int32_t ___m_IsNone;
};
struct ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 
{
	intptr_t ___m_Ptr;
};
struct StyleColor_tFC32BA34A15742AC48D6AACF8A137A6F71F04910 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFontDefinition_t0E1130277B322724A677D489018D219F014070F4 
{
	int32_t ___m_Keyword;
	FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C ___m_Value;
};
struct StyleFontDefinition_t0E1130277B322724A677D489018D219F014070F4_marshaled_pinvoke
{
	int32_t ___m_Keyword;
	FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C_marshaled_pinvoke ___m_Value;
};
struct StyleFontDefinition_t0E1130277B322724A677D489018D219F014070F4_marshaled_com
{
	int32_t ___m_Keyword;
	FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C_marshaled_com ___m_Value;
};
struct StyleLength_tF02B24735FC88BE29BEB36F7A87709CA28AF72D8 
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Value;
	int32_t ___m_Keyword;
};
struct TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___offset;
	float ___blurRadius;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color;
};
struct Touch_t03E51455ED508492B3F278903A0114FA0E87B417 
{
	int32_t ___m_FingerId;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Position;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RawPosition;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PositionDelta;
	float ___m_TimeDelta;
	int32_t ___m_TapCount;
	int32_t ___m_Phase;
	int32_t ___m_Type;
	float ___m_Pressure;
	float ___m_maximumPossiblePressure;
	float ___m_Radius;
	float ___m_RadiusVariance;
	float ___m_AltitudeAngle;
	float ___m_AzimuthAngle;
};
struct TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
	float ___m_Z;
};
struct Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E 
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
	float ___m_Z;
	bool ___m_isNone;
};
struct Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E_marshaled_pinvoke
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
	float ___m_Z;
	int32_t ___m_isNone;
};
struct Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E_marshaled_com
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
	float ___m_Z;
	int32_t ___m_isNone;
};
struct UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___cursorPos;
	float ___charWidth;
};
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___tangent;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv0;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv1;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv2;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv3;
};
struct BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357 
{
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___src;
	RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 ___srcRect;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___dstPos;
	int32_t ___border;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___tint;
};
struct BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357_marshaled_pinvoke
{
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___src;
	RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 ___srcRect;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___dstPos;
	int32_t ___border;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___tint;
};
struct BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357_marshaled_com
{
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___src;
	RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 ___srcRect;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___dstPos;
	int32_t ___border;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___tint;
};
struct Nullable_1_t5127ABE6809BA32727C69CB2E076B28D676EB15B 
{
	bool ___hasValue;
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 ___value;
};
struct ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D 
{
	ByReference_1_t9C85BCCAAF8C525B6C06B07E922D8D217BE8D6FC ____pointer;
	int32_t ____length;
};
struct ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 
{
	ByReference_1_t7BA5A6CA164F770BC688F21C5978D368716465F5 ____pointer;
	int32_t ____length;
};
struct Span_1_tDADAC65069DFE6B57C458109115ECD795ED39305 
{
	ByReference_1_t9C85BCCAAF8C525B6C06B07E922D8D217BE8D6FC ____pointer;
	int32_t ____length;
};
struct Span_1_tEDDF15FCF9EC6DEBA0F696BAACDDBAB9D92C252D 
{
	ByReference_1_t7BA5A6CA164F770BC688F21C5978D368716465F5 ____pointer;
	int32_t ____length;
};
struct Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 
{
	ByReference_1_t98B79BFB40A2CA0814BC183B09B4339A5EBF8524 ____pointer;
	int32_t ____length;
};
struct BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 
{
	NativeArray_1_t4020B6981295FB915DCE82EF368535F680C13A49 ___cullingPlanes;
	NativeArray_1_t73992261AA60020B6BE20D83C50B3F925CC89F31 ___cullingSplits;
	LODParameters_t54D2AA0FD8E53BCF51D7A42BC1A72FCA8C78A08A ___lodParameters;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___localToWorldMatrix;
	int32_t ___viewType;
	int32_t ___projectionType;
	int32_t ___cullingFlags;
	BatchPackedCullingViewID_t1E7EE8631C02555CAA181FA566CDC604B9FEFEBB ___viewID;
	uint32_t ___cullingLayerMask;
	uint64_t ___sceneCullingMask;
	uint8_t ___isOrthographic;
	int32_t ___receiverPlaneOffset;
	int32_t ___receiverPlaneCount;
};
struct CameraState_tBC57F8D313D0D19718B24CFBD690C089C2140156 
{
	LensSettings_t6DAB2F204EC22686BF4397E0871B4875414A84FE ___Lens;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___ReferenceUp;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___ReferenceLookAt;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___RawPosition;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___RawOrientation;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___PositionDampingBypass;
	float ___ShotQuality;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___PositionCorrection;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___OrientationCorrection;
	int32_t ___BlendHint;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom0;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom1;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom2;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom3;
	List_1_tECB13E82883EA864AEBA60A256302E1C8CFD6EF4* ___m_CustomOverflow;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField;
};
struct CameraState_tBC57F8D313D0D19718B24CFBD690C089C2140156_marshaled_pinvoke
{
	LensSettings_t6DAB2F204EC22686BF4397E0871B4875414A84FE_marshaled_pinvoke ___Lens;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___ReferenceUp;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___ReferenceLookAt;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___RawPosition;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___RawOrientation;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___PositionDampingBypass;
	float ___ShotQuality;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___PositionCorrection;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___OrientationCorrection;
	int32_t ___BlendHint;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom0;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom1;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom2;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom3;
	List_1_tECB13E82883EA864AEBA60A256302E1C8CFD6EF4* ___m_CustomOverflow;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField;
};
struct CameraState_tBC57F8D313D0D19718B24CFBD690C089C2140156_marshaled_com
{
	LensSettings_t6DAB2F204EC22686BF4397E0871B4875414A84FE_marshaled_com ___Lens;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___ReferenceUp;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___ReferenceLookAt;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___RawPosition;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___RawOrientation;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___PositionDampingBypass;
	float ___ShotQuality;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___PositionCorrection;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___OrientationCorrection;
	int32_t ___BlendHint;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom0;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom1;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom2;
	CustomBlendable_t99FF1C1C42F08A7265E2842451D5CB2F4BFF16CB ___mCustom3;
	List_1_tECB13E82883EA864AEBA60A256302E1C8CFD6EF4* ___m_CustomOverflow;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField;
};
struct MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD 
{
	void* ____pointer;
	GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC ____handle;
	RuntimeObject* ____pinnable;
};
struct MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD_marshaled_pinvoke
{
	void* ____pointer;
	GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC ____handle;
	RuntimeObject* ____pinnable;
};
struct MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD_marshaled_com
{
	void* ____pointer;
	GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC ____handle;
	RuntimeObject* ____pinnable;
};
struct Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F 
{
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle;
};
struct PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4 
{
	String_t* ___m_StreamName;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___m_SourceObject;
	Type_t* ___m_SourceBindingType;
	CreateOutputMethod_tD18AFE3B69E6DDD913D82D5FA1D5D909CEEC8509* ___m_CreateOutputMethod;
};
struct PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4_marshaled_pinvoke
{
	char* ___m_StreamName;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke ___m_SourceObject;
	Type_t* ___m_SourceBindingType;
	Il2CppMethodPointer ___m_CreateOutputMethod;
};
struct PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4_marshaled_com
{
	Il2CppChar* ___m_StreamName;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com* ___m_SourceObject;
	Type_t* ___m_SourceBindingType;
	Il2CppMethodPointer ___m_CreateOutputMethod;
};
struct PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 
{
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle;
};
struct StyleBackgroundSize_t0904929E2E236696CEC8DBD4B1082E8313F84008 
{
	BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610 
{
	Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610_marshaled_pinvoke
{
	Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82_marshaled_pinvoke ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610_marshaled_com
{
	Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82_marshaled_com ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B 
{
	Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B_marshaled_pinvoke
{
	Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7_marshaled_pinvoke ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B_marshaled_com
{
	Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7_marshaled_com ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC 
{
	Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC_marshaled_pinvoke
{
	Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7_marshaled_pinvoke ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC_marshaled_com
{
	Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7_marshaled_com ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTextShadow_tCDDF1FE733ADBAA5ACA3B74620D4728E83F54252 
{
	int32_t ___m_Keyword;
	TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 ___m_Value;
};
struct StyleTransformOrigin_t708B2E73541ECAE23D286FE68D6BC2CCFAAB84A6 
{
	TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089 
{
	Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089_marshaled_pinvoke
{
	Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E_marshaled_pinvoke ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089_marshaled_com
{
	Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E_marshaled_com ___m_Value;
	int32_t ___m_Keyword;
};
struct TypedReference_tF20A82297BED597FD80BDA0E41F74746B0FD642B 
{
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___type;
	intptr_t ___Value;
	intptr_t ___Type;
};
struct RectangleParams_t0B5A63548DC33EE252AF81E242B719118C235A4B 
{
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___uv;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___subRect;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionX;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionY;
	BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F ___backgroundRepeat;
	BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 ___backgroundSize;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___sprite;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___vectorImage;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material;
	int32_t ___scaleMode;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___playmodeTintColor;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___contentSize;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___textureSize;
	int32_t ___leftSlice;
	int32_t ___topSlice;
	int32_t ___rightSlice;
	int32_t ___bottomSlice;
	float ___sliceScale;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___spriteGeomRect;
	ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0 ___colorPage;
	int32_t ___meshFlags;
};
struct RectangleParams_t0B5A63548DC33EE252AF81E242B719118C235A4B_marshaled_pinvoke
{
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___uv;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___subRect;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionX;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionY;
	BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F ___backgroundRepeat;
	BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 ___backgroundSize;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___sprite;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___vectorImage;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material;
	int32_t ___scaleMode;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___playmodeTintColor;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___contentSize;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___textureSize;
	int32_t ___leftSlice;
	int32_t ___topSlice;
	int32_t ___rightSlice;
	int32_t ___bottomSlice;
	float ___sliceScale;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___spriteGeomRect;
	ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0_marshaled_pinvoke ___colorPage;
	int32_t ___meshFlags;
};
struct RectangleParams_t0B5A63548DC33EE252AF81E242B719118C235A4B_marshaled_com
{
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___uv;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___subRect;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionX;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionY;
	BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F ___backgroundRepeat;
	BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 ___backgroundSize;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___sprite;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___vectorImage;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material;
	int32_t ___scaleMode;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___playmodeTintColor;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___contentSize;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___textureSize;
	int32_t ___leftSlice;
	int32_t ___topSlice;
	int32_t ___rightSlice;
	int32_t ___bottomSlice;
	float ___sliceScale;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___spriteGeomRect;
	ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0_marshaled_com ___colorPage;
	int32_t ___meshFlags;
};
struct FrameData_t02E705D0271F73A24ADF9BA4B6F8760B6696F314 
{
	uint64_t ___m_FrameID;
	double ___m_DeltaTime;
	float ___m_Weight;
	float ___m_EffectiveWeight;
	double ___m_EffectiveParentDelay;
	float ___m_EffectiveParentSpeed;
	float ___m_EffectiveSpeed;
	int32_t ___m_Flags;
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 ___m_Output;
};
struct ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093_StaticFields
{
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___U3CEmptyU3Ek__BackingField;
};
struct JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5_StaticFields
{
	JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5 ___Empty;
};
struct ValueTask_1_t823DE87C36EA952D24C4E64F532E9D4425B72F21_StaticFields
{
	Task_1_t4C228DE57804012969575431CFF12D57C875552D* ___s_canceledTask;
};
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_StaticFields
{
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___s_actionToActionObjShunt;
};
struct CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B_StaticFields
{
	AnimationCurveU5BU5D_t2C4A38D7EFA8095F32316A4D9CE4CBB6840FB7EC* ___sStandardCurves;
};
struct CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257_StaticFields
{
	CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 ___Default;
};
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_StaticFields
{
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth365;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth366;
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MinValue;
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MaxValue;
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___UnixEpoch;
};
struct Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F_StaticFields
{
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___Zero;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___One;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MinusOne;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MaxValue;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MinValue;
};
struct Guid_t_StaticFields
{
	Guid_t ___Empty;
};
struct IntPtr_t_StaticFields
{
	intptr_t ___Zero;
};
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix;
};
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion;
};
struct TextureId_tFF4B4AAE53408AB10B0B89CCA5F7B50CF2535E58_StaticFields
{
	TextureId_tFF4B4AAE53408AB10B0B89CCA5F7B50CF2535E58 ___invalid;
};
struct TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A_StaticFields
{
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___Zero;
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___MaxValue;
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___MinValue;
};
struct ValueTask_t10B4B5DDF5C582607D0E634FA912F8CB94FCD49F_StaticFields
{
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___s_canceledTask;
};
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector;
};
struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_StaticFields
{
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Zero;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_One;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Up;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Down;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Left;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Right;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector;
};
struct Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376_StaticFields
{
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Zero;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_One;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Up;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Down;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Left;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Right;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Forward;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Back;
};
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector;
};
struct DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4_StaticFields
{
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 ___MinValue;
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 ___MaxValue;
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 ___UnixEpoch;
};
struct ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36_StaticFields
{
	ShaderTagId_t453E2085B5EE9448FF75E550CAB111EFF690ECB0 ___kRenderTypeTag;
};
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields
{
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___s_DefaultColor;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___s_DefaultTangent;
	UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 ___simpleVert;
};
struct CameraState_tBC57F8D313D0D19718B24CFBD690C089C2140156_StaticFields
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___kNoPoint;
};
struct Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F_StaticFields
{
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F ___m_NullPlayable;
};
struct PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4_StaticFields
{
	PlayableBindingU5BU5D_tC50C3F27A8E4246488F7A5998CAABAC4811A92CD* ___None;
	double ___DefaultDuration;
};
struct PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680_StaticFields
{
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 ___m_NullPlayableOutput;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif



static  ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 UnresolvedVirtualCall_0 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5 UnresolvedVirtualCall_1 (RuntimeObject* __this, const RuntimeMethod* method)
{
	JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  KeyValuePair_2_tDC26B09C26BA829DDE331BCB6AF7C508C763D7A3 UnresolvedVirtualCall_2 (RuntimeObject* __this, const RuntimeMethod* method)
{
	KeyValuePair_2_tDC26B09C26BA829DDE331BCB6AF7C508C763D7A3 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 UnresolvedVirtualCall_3 (RuntimeObject* __this, const RuntimeMethod* method)
{
	KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF UnresolvedVirtualCall_4 (RuntimeObject* __this, const RuntimeMethod* method)
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 UnresolvedVirtualCall_5 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_tEADC262F7F8B8BC4CC0A003DBDD3CA7C1B63F9AC UnresolvedVirtualCall_6 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_tEADC262F7F8B8BC4CC0A003DBDD3CA7C1B63F9AC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t5127ABE6809BA32727C69CB2E076B28D676EB15B UnresolvedVirtualCall_7 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_t5127ABE6809BA32727C69CB2E076B28D676EB15B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB UnresolvedVirtualCall_8 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 UnresolvedVirtualCall_9 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 UnresolvedVirtualCall_10 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D UnresolvedVirtualCall_11 (RuntimeObject* __this, const RuntimeMethod* method)
{
	ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 UnresolvedVirtualCall_12 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 UnresolvedVirtualCall_13 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleEnum_1_t3DD2EBD4E359AFE77C2974ECAA1DEE50E0FACEDC UnresolvedVirtualCall_14 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleEnum_1_t3DD2EBD4E359AFE77C2974ECAA1DEE50E0FACEDC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTask_1_t823DE87C36EA952D24C4E64F532E9D4425B72F21 UnresolvedVirtualCall_15 (RuntimeObject* __this, Memory_1_tB7CEF4416F5014E364267478CEF016A4AC5C0036 p1, CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	ValueTask_1_t823DE87C36EA952D24C4E64F532E9D4425B72F21 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTuple_2_tB358DB210B9947851BE1C2586AD7532BEB639942 UnresolvedVirtualCall_16 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	ValueTuple_2_tB358DB210B9947851BE1C2586AD7532BEB639942 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  BoundingSphere_t2DDB3D1711A6920C0ECA9217D3E4E14AFF03C010 UnresolvedVirtualCall_17 (RuntimeObject* __this, const RuntimeMethod* method)
{
	BoundingSphere_t2DDB3D1711A6920C0ECA9217D3E4E14AFF03C010 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 UnresolvedVirtualCall_18 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 UnresolvedVirtualCall_19 (RuntimeObject* __this, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  BoundsInt_t4E757DE5EFF9FCB42000F173360DDC63B5585485 UnresolvedVirtualCall_20 (RuntimeObject* __this, const RuntimeMethod* method)
{
	BoundsInt_t4E757DE5EFF9FCB42000F173360DDC63B5585485 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_21 (RuntimeObject* __this, const RuntimeMethod* method)
{
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_21 (void* __this, const RuntimeMethod* method)
{
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_22 (RuntimeObject* __this, CustomStyleProperty_1_tE4B20CAB5BCFEE711EB4A26F077DC700987C0C2D p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_23 (RuntimeObject* __this, CustomStyleProperty_1_t6871E5DBF19AB4DC7E1134B32A03B7A458D52E9F p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_24 (RuntimeObject* __this, CustomStyleProperty_1_t697913D630F101B4E464B7E9EA06368015C9C266 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_25 (RuntimeObject* __this, CustomStyleProperty_1_t21332918528099194FD36C74FF0FA14696F39493 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_26 (RuntimeObject* __this, KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_27 (RuntimeObject* __this, Span_1_tEDDF15FCF9EC6DEBA0F696BAACDDBAB9D92C252D p1, RuntimeObject* p2, ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_28 (RuntimeObject* __this, uint8_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_29 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_30 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_31 (RuntimeObject* __this, Guid_t p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_32 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_32 (void* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_32 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_33 (RuntimeObject* __this, int32_t p1, Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 p2, Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_34 (RuntimeObject* __this, int32_t p1, BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 p2, BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_35 (RuntimeObject* __this, int32_t p1, BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F p2, BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_36 (RuntimeObject* __this, int32_t p1, BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 p2, BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_37 (RuntimeObject* __this, int32_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_38 (RuntimeObject* __this, int32_t p1, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p2, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_39 (RuntimeObject* __this, int32_t p1, FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C p2, FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_40 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_41 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_42 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_43 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6,&p7,&p8,p9};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_44 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_45 (RuntimeObject* __this, int32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_46 (RuntimeObject* __this, int32_t p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_47 (RuntimeObject* __this, int32_t p1, Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 p2, Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_48 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_48 (void* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_49 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, uint8_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_50 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_51 (RuntimeObject* __this, int32_t p1, Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 p2, Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_52 (RuntimeObject* __this, int32_t p1, Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 p2, Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_53 (RuntimeObject* __this, int32_t p1, float p2, float p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_54 (RuntimeObject* __this, int32_t p1, TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 p2, TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_55 (RuntimeObject* __this, int32_t p1, TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 p2, TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_56 (RuntimeObject* __this, int32_t p1, Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E p2, Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_57 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_58 (RuntimeObject* __this, int64_t p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_59 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_59 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_59 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_60 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_61 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_62 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_62 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_63 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 p3, Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_64 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 p3, BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_65 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F p3, BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_66 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 p3, BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_67 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p3, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_68 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C p3, FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_69 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_70 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_71 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_72 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 p3, Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_73 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_73 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_74 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_75 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_76 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 p3, Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_77 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 p3, Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_78 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, float p3, float p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_79 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 p3, TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_80 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 p3, TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_81 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E p3, Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_82 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_82 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_83 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, uint8_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6,p7,p8};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_84 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_84 (void* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_84 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_85 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_86 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_86 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_87 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_87 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_88 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_89 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_90 (RuntimeObject* p1, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p2, RuntimeObject* p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_91 (RuntimeObject* __this, RuntimeObject* p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_92 (RuntimeObject* __this, RuntimeObject* p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, float p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_93 (RuntimeObject* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, RuntimeObject* p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_93 (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, RuntimeObject* p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_94 (RuntimeObject* __this, RuleMatcher_t327CFEB02C81AA20E639DE949DCBBAB5E92FF28E p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_95 (RuntimeObject* __this, float p1, int32_t p2, int32_t p3, uint8_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_96 (RuntimeObject* __this, float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_97 (RuntimeObject* __this, uint16_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_98 (RuntimeObject* __this, uint16_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_99 (RuntimeObject* __this, uint16_t p1, uint16_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_100 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_101 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_102 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_103 (RuntimeObject* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CameraState_tBC57F8D313D0D19718B24CFBD690C089C2140156 UnresolvedVirtualCall_104 (RuntimeObject* __this, const RuntimeMethod* method)
{
	CameraState_tBC57F8D313D0D19718B24CFBD690C089C2140156 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B UnresolvedVirtualCall_105 (RuntimeObject* __this, RuntimeObject* p1, CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B UnresolvedInstanceCall_105 (void* __this, RuntimeObject* p1, CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B UnresolvedVirtualCall_106 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B UnresolvedStaticCall_106 (RuntimeObject* p1, RuntimeObject* p2, CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B UnresolvedStaticCall_107 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	CinemachineBlendDefinition_t6A16D5B51D440E317D413EC8612647EDA0A6580B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Color_tD001788D726C3A7F1379BEED0260B9591F440C1F UnresolvedVirtualCall_108 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900 UnresolvedVirtualCall_109 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CoreRegistration_tD2BD53556CAA48BD5E0D32CB92C6494C0EB85581 UnresolvedVirtualCall_110 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	CoreRegistration_tD2BD53556CAA48BD5E0D32CB92C6494C0EB85581 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02 UnresolvedVirtualCall_111 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F UnresolvedVirtualCall_112 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9 UnresolvedVirtualCall_113 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_114 (RuntimeObject* __this, const RuntimeMethod* method)
{
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_115 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_116 (RuntimeObject* __this, DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_117 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_118 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_119 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6,&p7};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_120 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6,&p7,&p8};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_121 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_122 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 UnresolvedVirtualCall_123 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 UnresolvedVirtualCall_124 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F UnresolvedVirtualCall_125 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F UnresolvedVirtualCall_126 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB UnresolvedVirtualCall_127 (RuntimeObject* __this, const RuntimeMethod* method)
{
	DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_128 (RuntimeObject* __this, const RuntimeMethod* method)
{
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_129 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_130 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_131 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_132 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_133 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 UnresolvedVirtualCall_134 (RuntimeObject* __this, const RuntimeMethod* method)
{
	EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Guid_t UnresolvedVirtualCall_135 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Guid_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 UnresolvedVirtualCall_136 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int16_t UnresolvedVirtualCall_137 (RuntimeObject* __this, const RuntimeMethod* method)
{
	int16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int16_t UnresolvedVirtualCall_138 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_139 (RuntimeObject* __this, const RuntimeMethod* method)
{
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_139 (void* __this, const RuntimeMethod* method)
{
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_139 (const RuntimeMethod* method)
{
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_140 (RuntimeObject* __this, ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D p1, Span_1_tEDDF15FCF9EC6DEBA0F696BAACDDBAB9D92C252D p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_141 (RuntimeObject* __this, ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D p1, Span_1_tEDDF15FCF9EC6DEBA0F696BAACDDBAB9D92C252D p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_142 (RuntimeObject* __this, ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 p1, Span_1_tDADAC65069DFE6B57C458109115ECD795ED39305 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_143 (RuntimeObject* __this, Span_1_tDADAC65069DFE6B57C458109115ECD795ED39305 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_144 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_145 (RuntimeObject* __this, uint8_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_146 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_147 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_148 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_149 (RuntimeObject* __this, int16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_150 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_151 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_151 (void* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_152 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_153 (RuntimeObject* __this, int32_t p1, int64_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_153 (void* __this, int32_t p1, int64_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_154 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_155 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, float p4, float p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_156 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_157 (RuntimeObject* __this, int64_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_158 (RuntimeObject* __this, intptr_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_158 (intptr_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_159 (RuntimeObject* __this, intptr_t p1, int32_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_159 (intptr_t p1, int32_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_160 (RuntimeObject* __this, intptr_t p1, int32_t p2, intptr_t p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_160 (intptr_t p1, int32_t p2, intptr_t p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_161 (RuntimeObject* __this, intptr_t p1, intptr_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_161 (intptr_t p1, intptr_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_162 (RuntimeObject* __this, intptr_t p1, intptr_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_162 (intptr_t p1, intptr_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_163 (RuntimeObject* __this, intptr_t p1, intptr_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_163 (intptr_t p1, intptr_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_164 (RuntimeObject* __this, intptr_t p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_164 (intptr_t p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_165 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_165 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_165 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_166 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_167 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_168 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_169 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_170 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_170 (RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_171 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_172 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_173 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_174 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, uint8_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_175 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, int32_t p6, int32_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,&p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_176 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_176 (RuntimeObject* p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_177 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_178 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_179 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_180 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_181 (RuntimeObject* p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_182 (RuntimeObject* p1, intptr_t p2, int32_t p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_183 (RuntimeObject* p1, intptr_t p2, int32_t p3, intptr_t p4, intptr_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_184 (RuntimeObject* p1, intptr_t p2, intptr_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_185 (RuntimeObject* p1, intptr_t p2, intptr_t p3, int32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_186 (RuntimeObject* p1, intptr_t p2, intptr_t p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_187 (RuntimeObject* p1, intptr_t p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_188 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_188 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_189 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_190 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_191 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_191 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_192 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_193 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int64_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_194 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_195 (RuntimeObject* p1, RuntimeObject* p2, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_196 (RuntimeObject* p1, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p2, RuntimeObject* p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_197 (RuntimeObject* __this, RuntimeObject* p1, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_197 (RuntimeObject* p1, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_198 (RuntimeObject* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, RuntimeObject* p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_198 (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, RuntimeObject* p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_199 (RuntimeObject* __this, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_199 (void* __this, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_200 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_201 (RuntimeObject* __this, const RuntimeMethod* method)
{
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedStaticCall_201 (const RuntimeMethod* method)
{
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_202 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_203 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_204 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_205 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_206 (RuntimeObject* __this, int64_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_207 (RuntimeObject* __this, intptr_t p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedStaticCall_207 (intptr_t p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_208 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedStaticCall_208 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_209 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_210 (RuntimeObject* __this, RuntimeObject* p1, double p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedStaticCall_211 (RuntimeObject* p1, intptr_t p2, int32_t p3, int64_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_212 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_213 (RuntimeObject* __this, const RuntimeMethod* method)
{
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_214 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_214 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_215 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_216 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_217 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedInstanceCall_217 (void* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_218 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_218 (RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_219 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_220 (RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_221 (RuntimeObject* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_221 (unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedVirtualCall_222 (RuntimeObject* __this, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p1, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedInstanceCall_222 (void* __this, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p1, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedVirtualCall_223 (RuntimeObject* __this, RuntimeObject* p1, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p2, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedStaticCall_223 (RuntimeObject* p1, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p2, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedStaticCall_224 (RuntimeObject* p1, RuntimeObject* p2, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p3, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p4, intptr_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD UnresolvedVirtualCall_225 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_226 (RuntimeObject* __this, const RuntimeMethod* method)
{
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_226 (void* __this, const RuntimeMethod* method)
{
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_226 (const RuntimeMethod* method)
{
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_227 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_228 (RuntimeObject* __this, uint8_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_229 (RuntimeObject* __this, CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_230 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_231 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_232 (RuntimeObject* __this, DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_233 (RuntimeObject* __this, Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_234 (RuntimeObject* __this, Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_235 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_236 (RuntimeObject* __this, double p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_237 (RuntimeObject* __this, Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_238 (RuntimeObject* __this, int16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_239 (RuntimeObject* __this, int16_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_240 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_241 (RuntimeObject* __this, int32_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_242 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_243 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_244 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_245 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_246 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_247 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_248 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4,p5,p6,p7};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_249 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_250 (RuntimeObject* __this, int64_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_251 (RuntimeObject* __this, intptr_t p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_251 (void* __this, intptr_t p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_252 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_252 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_252 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_253 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_254 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_255 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_256 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_257 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_257 (void* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_258 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_259 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_260 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_261 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, LoadSceneParameters_tFBAFEA7FA75F282D3034241AD8756A7B5578310E p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_262 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_263 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_264 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_265 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5,p6,p7,p8};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_266 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_266 (RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_267 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_267 (void* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_267 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_268 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_269 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_270 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_270 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_271 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_272 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint8_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_273 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_274 (RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_275 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_275 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_275 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_276 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_277 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_278 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_278 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_278 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_279 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_279 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_279 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_280 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,&p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_281 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_281 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_281 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_282 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_282 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_282 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_283 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_283 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_283 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_284 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_284 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_284 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_285 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_285 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_285 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_286 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_286 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_286 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_287 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_287 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_287 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_288 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_288 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_288 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_289 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_289 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_289 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_290 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_290 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_290 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_291 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_291 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_292 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, RuntimeObject* p17, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_293 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_294 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_294 (RuntimeObject* p1, RuntimeObject* p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_295 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_296 (RuntimeObject* p1, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_297 (RuntimeObject* __this, RuntimeObject* p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_297 (void* __this, RuntimeObject* p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_298 (RuntimeObject* __this, RuntimeObject* p1, float p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_299 (RuntimeObject* __this, RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_300 (RuntimeObject* p1, uint32_t p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_301 (RuntimeObject* p1, unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p2, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_302 (RuntimeObject* p1, unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p2, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p3, uint64_t p4, uint64_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_303 (RuntimeObject* __this, PropertyName_tE4B4AAA58AF3BF2C0CD95509EB7B786F096901C2 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_304 (RuntimeObject* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, float p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_304 (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, float p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_305 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_306 (RuntimeObject* __this, float p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_307 (RuntimeObject* __this, float p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_308 (RuntimeObject* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_309 (RuntimeObject* __this, StyleValues_t4AED947A53B84B62EF2B589A40B74911CA77D11A p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_310 (RuntimeObject* __this, TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_311 (RuntimeObject* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_312 (RuntimeObject* __this, uint32_t p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_312 (uint32_t p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_313 (RuntimeObject* __this, uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_314 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_315 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_316 (RuntimeObject* __this, unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p1, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_316 (unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p1, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_317 (RuntimeObject* __this, unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p1, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p2, uint64_t p3, uint64_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_317 (unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p1, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p2, uint64_t p3, uint64_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  PenData_t2345B5FBD18D851528C5C18F8A667D4EF4690945 UnresolvedVirtualCall_318 (RuntimeObject* __this, const RuntimeMethod* method)
{
	PenData_t2345B5FBD18D851528C5C18F8A667D4EF4690945 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F UnresolvedVirtualCall_319 (RuntimeObject* __this, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F UnresolvedVirtualCall_320 (RuntimeObject* __this, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F UnresolvedVirtualCall_321 (RuntimeObject* __this, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F UnresolvedVirtualCall_322 (RuntimeObject* __this, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4 UnresolvedVirtualCall_323 (RuntimeObject* __this, const RuntimeMethod* method)
{
	PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 UnresolvedStaticCall_324 (RuntimeObject* p1, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 UnresolvedVirtualCall_325 (RuntimeObject* __this, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 UnresolvedStaticCall_325 (PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD UnresolvedVirtualCall_326 (RuntimeObject* __this, const RuntimeMethod* method)
{
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 UnresolvedVirtualCall_327 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF UnresolvedVirtualCall_328 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Range_tDDBAD7CBDC5DD273DA4330F4E9CDF24C62F16ED6 UnresolvedVirtualCall_329 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Range_tDDBAD7CBDC5DD273DA4330F4E9CDF24C62F16ED6 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA UnresolvedStaticCall_330 (RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA UnresolvedVirtualCall_331 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA UnresolvedStaticCall_331 (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D UnresolvedVirtualCall_332 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 UnresolvedVirtualCall_333 (RuntimeObject* __this, const RuntimeMethod* method)
{
	RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeMethodHandle_tB35B96E97214DCBE20B0B02B1E687884B34680B2 UnresolvedVirtualCall_334 (RuntimeObject* __this, const RuntimeMethod* method)
{
	RuntimeMethodHandle_tB35B96E97214DCBE20B0B02B1E687884B34680B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B UnresolvedVirtualCall_335 (RuntimeObject* __this, const RuntimeMethod* method)
{
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int8_t UnresolvedVirtualCall_336 (RuntimeObject* __this, const RuntimeMethod* method)
{
	int8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int8_t UnresolvedVirtualCall_337 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 UnresolvedVirtualCall_338 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  SerializableProjectConfiguration_tBAE4D3A66EC38C1869E294396DB79F127B8F58EE UnresolvedVirtualCall_339 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	SerializableProjectConfiguration_tBAE4D3A66EC38C1869E294396DB79F127B8F58EE il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_340 (RuntimeObject* __this, const RuntimeMethod* method)
{
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedInstanceCall_340 (void* __this, const RuntimeMethod* method)
{
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_341 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_342 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_343 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_343 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_344 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_345 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_346 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_347 (RuntimeObject* p1, RuntimeObject* p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_348 (RuntimeObject* p1, RuntimeObject* p2, float p3, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_349 (RuntimeObject* __this, RuntimeObject* p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_349 (RuntimeObject* p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_350 (RuntimeObject* p1, float p2, float p3, float p4, float p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_351 (RuntimeObject* __this, RuntimeObject* p1, float p2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_351 (RuntimeObject* p1, float p2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_352 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_353 (RuntimeObject* __this, float p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_354 (RuntimeObject* __this, float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedInstanceCall_354 (void* __this, float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_355 (RuntimeObject* __this, float p1, float p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_355 (float p1, float p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_356 (RuntimeObject* __this, float p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedInstanceCall_356 (void* __this, float p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_357 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 UnresolvedVirtualCall_358 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 UnresolvedVirtualCall_359 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 UnresolvedVirtualCall_360 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleBackgroundSize_t0904929E2E236696CEC8DBD4B1082E8313F84008 UnresolvedVirtualCall_361 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleBackgroundSize_t0904929E2E236696CEC8DBD4B1082E8313F84008 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610 UnresolvedVirtualCall_362 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleFloat_t4A100BCCDC275C2302517C5858C9BE9EC43D4841 UnresolvedVirtualCall_363 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleFloat_t4A100BCCDC275C2302517C5858C9BE9EC43D4841 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleLength_tF02B24735FC88BE29BEB36F7A87709CA28AF72D8 UnresolvedVirtualCall_364 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleLength_tF02B24735FC88BE29BEB36F7A87709CA28AF72D8 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B UnresolvedVirtualCall_365 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC UnresolvedVirtualCall_366 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleTextShadow_tCDDF1FE733ADBAA5ACA3B74620D4728E83F54252 UnresolvedVirtualCall_367 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleTextShadow_tCDDF1FE733ADBAA5ACA3B74620D4728E83F54252 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleTransformOrigin_t708B2E73541ECAE23D286FE68D6BC2CCFAAB84A6 UnresolvedVirtualCall_368 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleTransformOrigin_t708B2E73541ECAE23D286FE68D6BC2CCFAAB84A6 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089 UnresolvedVirtualCall_369 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedVirtualCall_370 (RuntimeObject* __this, const RuntimeMethod* method)
{
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedInstanceCall_370 (void* __this, const RuntimeMethod* method)
{
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedVirtualCall_371 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedStaticCall_371 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedStaticCall_372 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedVirtualCall_373 (RuntimeObject* __this, TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Touch_t03E51455ED508492B3F278903A0114FA0E87B417 UnresolvedVirtualCall_374 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Touch_t03E51455ED508492B3F278903A0114FA0E87B417 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD UnresolvedVirtualCall_375 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC UnresolvedVirtualCall_376 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 UnresolvedVirtualCall_377 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_378 (RuntimeObject* __this, const RuntimeMethod* method)
{
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_379 (RuntimeObject* __this, int32_t p1, uint16_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedInstanceCall_379 (void* __this, int32_t p1, uint16_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_380 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_381 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, uint16_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedStaticCall_381 (RuntimeObject* p1, int32_t p2, uint16_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedStaticCall_382 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint16_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_383 (RuntimeObject* __this, uint16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_384 (RuntimeObject* __this, const RuntimeMethod* method)
{
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_385 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_385 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_386 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_386 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_387 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_388 (RuntimeObject* p1, RuntimeObject* p2, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_389 (RuntimeObject* p1, RuntimeObject* p2, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_390 (RuntimeObject* __this, RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_390 (RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_391 (RuntimeObject* __this, RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_391 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_392 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5,p6,p7};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_393 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,p6,p7,p8};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_394 (RuntimeObject* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_394 (void* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_395 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_395 (void* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_396 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_396 (unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_397 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5,p6,p7};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_397 (unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5,p6,p7};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_398 (RuntimeObject* __this, const RuntimeMethod* method)
{
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_399 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTask_t10B4B5DDF5C582607D0E634FA912F8CB94FCD49F UnresolvedVirtualCall_400 (RuntimeObject* __this, ReadOnlyMemory_1_t63F301BF893B0AB689953D86A641168CA66D2399 p1, CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	ValueTask_t10B4B5DDF5C582607D0E634FA912F8CB94FCD49F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 UnresolvedVirtualCall_401 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 UnresolvedVirtualCall_402 (RuntimeObject* __this, RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, uint8_t p3, int32_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5,p6};
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 UnresolvedVirtualCall_403 (RuntimeObject* __this, float p1, int32_t p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_404 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_405 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_406 (RuntimeObject* __this, float p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_407 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_408 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  void UnresolvedVirtualCall_409 (RuntimeObject* __this, const RuntimeMethod* method)
{
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, NULL);
}
static  void UnresolvedInstanceCall_409 (void* __this, const RuntimeMethod* method)
{
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, NULL);
}
static  void UnresolvedStaticCall_409 (const RuntimeMethod* method)
{
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, NULL);
}
static  void UnresolvedVirtualCall_410 (RuntimeObject* __this, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_410 (void* __this, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_411 (RuntimeObject* __this, Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_412 (RuntimeObject* __this, Nullable_1_tEB6689CC9747A3600689077DCBF77B8E8B510505 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_413 (RuntimeObject* __this, Nullable_1_tD52F1D0FC7EBB336F119BE953E59F426766032C1 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_414 (RuntimeObject* __this, Nullable_1_tEADC262F7F8B8BC4CC0A003DBDD3CA7C1B63F9AC p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_415 (RuntimeObject* __this, Nullable_1_t5127ABE6809BA32727C69CB2E076B28D676EB15B p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_416 (RuntimeObject* __this, Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_417 (RuntimeObject* __this, Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_418 (RuntimeObject* __this, Nullable_1_t0ECB838EB0C9A81655750B26970F21CF9A83A5F7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_419 (RuntimeObject* __this, Nullable_1_t57D99A484501B89DA27E67D6D9A89722D5A7DE2C p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_420 (RuntimeObject* __this, Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_421 (RuntimeObject* __this, Nullable_1_t365991B3904FDA7642A788423B28692FDC7CDB17 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_422 (RuntimeObject* __this, Nullable_1_tCF16C2638810B89EAA3EEFE6B35FC71B6AE96B2C p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_423 (RuntimeObject* __this, Nullable_1_t3D746CBB6123D4569FF4DEA60BC4240F32C6FE75 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_424 (RuntimeObject* __this, Nullable_1_tE151CE1F6892804B41C4004C95CB57020ABB3272 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_425 (RuntimeObject* __this, Nullable_1_t70F850DEE49B62D1B877D3C32F9E0EC724ADC4D9 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_426 (RuntimeObject* __this, Nullable_1_tD043F01310E483091D0E9A5526C3425F13EF2099 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_427 (RuntimeObject* __this, Nullable_1_tF8BFF19FF240C9F0A45168187CD7106BAA146A99 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_428 (RuntimeObject* __this, ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_429 (RuntimeObject* __this, ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_430 (RuntimeObject* __this, Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_430 (Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_431 (RuntimeObject* __this, StyleEnum_1_t3DD2EBD4E359AFE77C2974ECAA1DEE50E0FACEDC p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_432 (RuntimeObject* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_433 (RuntimeObject* __this, BoundsInt_t4E757DE5EFF9FCB42000F173360DDC63B5585485 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_434 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_434 (void* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_434 (uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_435 (RuntimeObject* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_436 (RuntimeObject* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p1, float p2, uint8_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_437 (RuntimeObject* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p1, float p2, uint8_t p3, uint8_t p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_438 (RuntimeObject* __this, CullingGroupEvent_tC79BA328A8280C29F6002F591614081A0E87D110 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_438 (CullingGroupEvent_tC79BA328A8280C29F6002F591614081A0E87D110 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_439 (RuntimeObject* __this, Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_440 (RuntimeObject* __this, DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_441 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_442 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_443 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_444 (RuntimeObject* __this, DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_445 (RuntimeObject* __this, Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_446 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_446 (void* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_447 (RuntimeObject* __this, double p1, double p2, FrameData_t02E705D0271F73A24ADF9BA4B6F8760B6696F314 p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_448 (RuntimeObject* __this, double p1, FrameData_t02E705D0271F73A24ADF9BA4B6F8760B6696F314 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_449 (RuntimeObject* __this, double p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_450 (RuntimeObject* __this, EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_451 (RuntimeObject* __this, Guid_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_452 (RuntimeObject* __this, Guid_t p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_453 (RuntimeObject* __this, Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_454 (RuntimeObject* __this, int16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_455 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_455 (void* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_455 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_456 (RuntimeObject* __this, int32_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_457 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_457 (void* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_458 (RuntimeObject* __this, int32_t p1, int32_t p2, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_459 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_460 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_461 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_462 (RuntimeObject* __this, int32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_463 (RuntimeObject* __this, int32_t p1, int64_t p2, int64_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_463 (int32_t p1, int64_t p2, int64_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_464 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_464 (void* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_464 (int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_465 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_466 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_467 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_468 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_469 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_469 (void* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_470 (RuntimeObject* __this, int64_t p1, RuntimeObject* p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_471 (RuntimeObject* __this, intptr_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_471 (intptr_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_472 (RuntimeObject* __this, intptr_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_472 (intptr_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_473 (RuntimeObject* __this, intptr_t p1, intptr_t p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_473 (void* __this, intptr_t p1, intptr_t p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_474 (RuntimeObject* __this, intptr_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_474 (void* __this, intptr_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_474 (intptr_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_475 (RuntimeObject* __this, intptr_t p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_475 (intptr_t p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_476 (RuntimeObject* __this, intptr_t p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_476 (intptr_t p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_477 (RuntimeObject* __this, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_478 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_478 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_478 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_479 (RuntimeObject* __this, RuntimeObject* p1, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_479 (RuntimeObject* p1, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_480 (RuntimeObject* p1, Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_481 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_481 (RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_482 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_483 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, uint8_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_484 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_485 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_486 (RuntimeObject* p1, CullingGroupEvent_tC79BA328A8280C29F6002F591614081A0E87D110 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_487 (RuntimeObject* __this, RuntimeObject* p1, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p2, Nullable_1_t365991B3904FDA7642A788423B28692FDC7CDB17 p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_488 (RuntimeObject* __this, RuntimeObject* p1, double p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_488 (RuntimeObject* p1, double p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_489 (RuntimeObject* __this, RuntimeObject* p1, double p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_490 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_490 (void* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_490 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_491 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_491 (RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_492 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, int32_t p6, uint8_t p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,&p6,&p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_493 (RuntimeObject* p1, int32_t p2, int64_t p3, int64_t p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_494 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_494 (void* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_494 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_495 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, uint8_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_496 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_497 (RuntimeObject* __this, RuntimeObject* p1, int64_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_497 (RuntimeObject* p1, int64_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_498 (RuntimeObject* p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_499 (RuntimeObject* p1, intptr_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_500 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, intptr_t p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_500 (RuntimeObject* p1, intptr_t p2, intptr_t p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_501 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_501 (void* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_501 (RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_502 (RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_503 (RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, int32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_504 (RuntimeObject* __this, RuntimeObject* p1, LineInfo_t415DCF0EAD0FB3806F779BA170EC9058E47CCB24 p2, LineInfo_t415DCF0EAD0FB3806F779BA170EC9058E47CCB24 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_505 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_505 (void* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_505 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_506 (RuntimeObject* p1, RuntimeObject* p2, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_507 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_507 (RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_508 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_509 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_510 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_511 (RuntimeObject* p1, RuntimeObject* p2, double p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_512 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int16_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_513 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_513 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_514 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_515 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_516 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_517 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_517 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_518 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_519 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_520 (RuntimeObject* p1, RuntimeObject* p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_521 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_521 (void* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_522 (RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, intptr_t p4, RuntimeObject* p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,p5,&p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_523 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_523 (RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_524 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_524 (void* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_525 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_525 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_525 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_526 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_527 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_528 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_528 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_529 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_530 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, intptr_t p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5,&p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_530 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, intptr_t p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5,&p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_531 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_531 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_531 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_532 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, uint8_t p5, uint8_t p6, uint8_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5,&p6,&p7};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_533 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, intptr_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_534 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, intptr_t p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5,p6,&p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_535 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_535 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_535 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_536 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_536 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_536 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_537 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_537 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_537 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_538 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_538 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_538 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_539 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_539 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_539 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_540 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_540 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_540 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_541 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_541 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_541 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_542 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_542 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_542 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_543 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_543 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_543 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_544 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_544 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_544 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_545 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_545 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_545 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_546 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_546 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_547 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, RuntimeObject* p17, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_548 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_549 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_549 (RuntimeObject* p1, RuntimeObject* p2, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_550 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_551 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_551 (RuntimeObject* p1, RuntimeObject* p2, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_552 (RuntimeObject* p1, RuntimeObject* p2, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_553 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, TextureId_tFF4B4AAE53408AB10B0B89CCA5F7B50CF2535E58 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_554 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_555 (RuntimeObject* p1, RuntimeObject* p2, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_556 (RuntimeObject* p1, RuntimeObject* p2, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_557 (RuntimeObject* p1, PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE p2, ReadOnly_t1D4689336F49F434532D72398BFBE7BF4D6059D4 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_558 (RuntimeObject* __this, RuntimeObject* p1, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_559 (RuntimeObject* __this, RuntimeObject* p1, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_559 (void* __this, RuntimeObject* p1, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_559 (RuntimeObject* p1, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_560 (RuntimeObject* __this, RuntimeObject* p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_561 (RuntimeObject* __this, RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_561 (RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_562 (RuntimeObject* __this, RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_562 (RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_563 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_563 (RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_564 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_565 (RuntimeObject* __this, RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_566 (RuntimeObject* __this, RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_567 (RuntimeObject* __this, RuntimeObject* p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_568 (RuntimeObject* __this, RuntimeObject* p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_569 (RuntimeObject* __this, RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_569 (RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_570 (RuntimeObject* __this, RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_570 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_571 (RuntimeObject* __this, PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE p1, ReadOnly_t1D4689336F49F434532D72398BFBE7BF4D6059D4 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_571 (PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE p1, ReadOnly_t1D4689336F49F434532D72398BFBE7BF4D6059D4 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_572 (RuntimeObject* __this, RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_573 (RuntimeObject* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_574 (RuntimeObject* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_575 (RuntimeObject* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_576 (RuntimeObject* __this, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_577 (RuntimeObject* __this, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_577 (void* __this, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_578 (RuntimeObject* __this, int8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_579 (RuntimeObject* __this, ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_580 (RuntimeObject* __this, ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_581 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_582 (RuntimeObject* __this, float p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_583 (RuntimeObject* __this, float p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_584 (RuntimeObject* __this, float p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_585 (RuntimeObject* __this, float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_586 (RuntimeObject* __this, float p1, float p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_587 (RuntimeObject* __this, float p1, float p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_588 (RuntimeObject* __this, StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_589 (RuntimeObject* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_589 (void* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_589 (StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_590 (RuntimeObject* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_590 (void* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_591 (RuntimeObject* __this, StyleColor_tFC32BA34A15742AC48D6AACF8A137A6F71F04910 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_592 (RuntimeObject* __this, StyleFloat_t4A100BCCDC275C2302517C5858C9BE9EC43D4841 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_593 (RuntimeObject* __this, StyleFont_t9D8A6F3E224B60FD8BA1522CE8AB0E2E8BE8B77C p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_594 (RuntimeObject* __this, StyleFontDefinition_t0E1130277B322724A677D489018D219F014070F4 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_595 (RuntimeObject* __this, StyleLength_tF02B24735FC88BE29BEB36F7A87709CA28AF72D8 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_596 (RuntimeObject* __this, StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_597 (RuntimeObject* __this, TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_598 (RuntimeObject* __this, TimerState_t82C7C29B095D6ACDC06AC172C269E9D5F0508ECE p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_599 (RuntimeObject* __this, TypedReference_tF20A82297BED597FD80BDA0E41F74746B0FD642B p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_600 (RuntimeObject* __this, uint16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_601 (RuntimeObject* __this, uint16_t p1, uint16_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_602 (RuntimeObject* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_602 (void* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_603 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_603 (uint32_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_604 (RuntimeObject* __this, uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_605 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_606 (RuntimeObject* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_607 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_608 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, double p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_609 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_610 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_611 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_612 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_613 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_614 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, uint64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_615 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_616 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_617 (RuntimeObject* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_618 (RuntimeObject* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_619 (RuntimeObject* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_620 (RuntimeObject* __this, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_621 (RuntimeObject* __this, RectangleParams_t0B5A63548DC33EE252AF81E242B719118C235A4B p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_622 (RuntimeObject* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_622 (void* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_623 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_623 (void* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedStaticCall_624 (RuntimeObject* p1, RuntimeObject* p2, float p3, int32_t p4, float p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5,&p6};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedVirtualCall_625 (RuntimeObject* __this, RuntimeObject* p1, float p2, int32_t p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedStaticCall_625 (RuntimeObject* p1, float p2, int32_t p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedVirtualCall_626 (RuntimeObject* __this, float p1, int32_t p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedInstanceCall_626 (void* __this, float p1, int32_t p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357 UnresolvedVirtualCall_627 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 UnresolvedVirtualCall_628 (RuntimeObject* __this, const RuntimeMethod* method)
{
	unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 UnresolvedStaticCall_628 (const RuntimeMethod* method)
{
	unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 UnresolvedStaticCall_629 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedVirtualCall_630 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedInstanceCall_630 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedVirtualCall_631 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedStaticCall_631 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedStaticCall_632 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 UnresolvedStaticCall_633 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 UnresolvedVirtualCall_634 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 UnresolvedStaticCall_634 (unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedVirtualCall_635 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedInstanceCall_635 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedVirtualCall_636 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedStaticCall_636 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedStaticCall_637 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
IL2CPP_EXTERN_C const Il2CppMethodPointer g_UnresolvedVirtualMethodPointers[];
const Il2CppMethodPointer g_UnresolvedVirtualMethodPointers[638] = 
{
	(const Il2CppMethodPointer)UnresolvedVirtualCall_0,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_1,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_2,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_3,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_4,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_5,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_6,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_7,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_8,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_9,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_10,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_11,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_12,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_13,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_14,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_15,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_16,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_17,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_18,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_19,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_20,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_21,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_22,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_23,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_24,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_25,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_26,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_27,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_28,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_29,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_30,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_31,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_32,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_33,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_34,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_35,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_36,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_37,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_38,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_39,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_40,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_41,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_42,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_43,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_44,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_45,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_46,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_47,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_48,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_49,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_50,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_51,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_52,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_53,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_54,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_55,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_56,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_57,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_58,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_59,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_60,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_61,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_62,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_63,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_64,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_65,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_66,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_67,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_68,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_69,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_70,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_71,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_72,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_73,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_74,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_75,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_76,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_77,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_78,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_79,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_80,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_81,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_82,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_83,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_84,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_86,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_87,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_88,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_91,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_92,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_93,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_94,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_95,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_96,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_97,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_98,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_99,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_100,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_101,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_102,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_103,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_104,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_105,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_106,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_108,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_109,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_110,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_111,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_112,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_113,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_114,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_115,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_116,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_117,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_118,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_119,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_120,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_121,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_122,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_123,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_124,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_125,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_126,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_127,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_128,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_129,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_130,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_131,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_132,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_133,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_134,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_135,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_136,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_137,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_138,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_139,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_140,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_141,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_142,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_143,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_144,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_145,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_146,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_147,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_148,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_149,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_150,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_151,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_152,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_153,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_154,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_155,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_156,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_157,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_158,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_159,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_160,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_161,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_162,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_163,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_164,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_165,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_166,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_167,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_168,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_169,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_170,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_171,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_172,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_173,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_174,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_175,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_176,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_177,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_178,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_179,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_180,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_188,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_189,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_190,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_191,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_192,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_197,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_198,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_199,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_200,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_201,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_202,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_203,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_204,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_205,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_206,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_207,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_208,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_209,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_210,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_212,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_213,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_214,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_216,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_217,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_218,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_221,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_222,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_223,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_225,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_226,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_227,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_228,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_229,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_230,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_231,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_232,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_233,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_234,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_235,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_236,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_237,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_238,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_239,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_240,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_241,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_242,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_243,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_244,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_245,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_246,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_247,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_248,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_249,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_250,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_251,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_252,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_253,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_254,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_255,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_256,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_257,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_258,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_259,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_260,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_261,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_262,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_263,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_264,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_265,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_266,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_267,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_268,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_269,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_270,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_271,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_272,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_273,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_275,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_276,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_278,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_279,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_280,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_281,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_282,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_283,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_284,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_285,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_286,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_287,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_288,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_289,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_290,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_291,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_294,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_295,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_297,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_298,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_299,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_303,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_304,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_305,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_306,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_307,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_308,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_309,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_310,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_311,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_312,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_313,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_314,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_315,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_316,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_317,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_318,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_319,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_320,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_321,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_322,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_323,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_325,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_326,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_327,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_328,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_329,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_331,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_332,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_333,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_334,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_335,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_336,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_337,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_338,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_339,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_340,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_341,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_342,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_343,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_344,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_346,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_349,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_351,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_352,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_353,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_354,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_355,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_356,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_357,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_358,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_359,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_360,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_361,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_362,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_363,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_364,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_365,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_366,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_367,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_368,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_369,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_370,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_371,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_373,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_374,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_375,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_376,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_377,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_378,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_379,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_380,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_381,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_383,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_384,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_385,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_386,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_390,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_391,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_394,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_395,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_396,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_397,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_398,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_399,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_400,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_401,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_402,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_403,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_404,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_405,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_406,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_407,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_408,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_409,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_410,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_411,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_412,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_413,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_414,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_415,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_416,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_417,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_418,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_419,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_420,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_421,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_422,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_423,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_424,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_425,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_426,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_427,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_428,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_429,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_430,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_431,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_432,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_433,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_434,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_435,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_436,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_437,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_438,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_439,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_440,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_441,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_442,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_443,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_444,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_445,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_446,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_447,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_448,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_449,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_450,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_451,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_452,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_453,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_454,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_455,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_456,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_457,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_458,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_459,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_460,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_461,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_462,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_463,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_464,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_465,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_466,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_467,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_468,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_469,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_470,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_471,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_472,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_473,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_474,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_475,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_476,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_477,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_478,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_479,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_481,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_482,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_483,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_484,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_485,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_487,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_488,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_489,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_490,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_491,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_492,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_494,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_495,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_496,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_497,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_500,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_501,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_504,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_505,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_507,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_508,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_509,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_510,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_512,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_513,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_514,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_516,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_517,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_518,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_519,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_521,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_523,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_524,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_525,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_528,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_530,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_531,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_532,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_535,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_536,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_537,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_538,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_539,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_540,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_541,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_542,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_543,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_544,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_545,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_546,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_549,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_550,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_551,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_553,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_558,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_559,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_560,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_561,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_562,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_563,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_565,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_566,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_567,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_568,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_569,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_570,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_571,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_572,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_573,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_574,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_575,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_576,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_577,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_578,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_579,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_580,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_581,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_582,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_583,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_584,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_585,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_586,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_587,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_588,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_589,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_590,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_591,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_592,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_593,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_594,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_595,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_596,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_597,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_598,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_599,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_600,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_601,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_602,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_603,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_604,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_605,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_606,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_607,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_608,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_609,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_610,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_611,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_612,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_613,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_614,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_615,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_616,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_617,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_618,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_619,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_620,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_621,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_622,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_623,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_625,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_626,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_627,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_628,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_630,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_631,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_634,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_635,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_636,
	NULL,
};
IL2CPP_EXTERN_C const Il2CppMethodPointer g_UnresolvedInstanceMethodPointers[];
const Il2CppMethodPointer g_UnresolvedInstanceMethodPointers[638] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_21,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_32,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_48,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_59,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_84,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_105,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_139,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_151,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_153,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_165,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_199,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_217,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_222,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_226,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_251,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_252,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_257,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_267,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_275,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_278,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_279,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_281,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_282,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_283,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_284,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_285,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_286,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_287,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_288,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_289,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_290,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_297,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_340,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_354,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_356,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_370,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_379,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_385,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_394,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_395,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_409,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_410,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_434,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_446,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_455,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_457,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_464,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_469,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_473,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_474,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_478,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_490,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_494,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_501,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_505,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_521,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_524,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_525,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_531,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_535,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_536,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_537,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_538,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_539,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_540,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_541,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_542,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_543,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_544,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_545,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_559,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_577,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_589,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_590,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_602,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_622,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_623,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_626,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_630,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_635,
	NULL,
	NULL,
};
IL2CPP_EXTERN_C const Il2CppMethodPointer g_UnresolvedStaticMethodPointers[];
const Il2CppMethodPointer g_UnresolvedStaticMethodPointers[638] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_32,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_59,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_62,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_73,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_82,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_84,
	(const Il2CppMethodPointer)UnresolvedStaticCall_85,
	(const Il2CppMethodPointer)UnresolvedStaticCall_86,
	(const Il2CppMethodPointer)UnresolvedStaticCall_87,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_89,
	(const Il2CppMethodPointer)UnresolvedStaticCall_90,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_93,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_106,
	(const Il2CppMethodPointer)UnresolvedStaticCall_107,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_139,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_158,
	(const Il2CppMethodPointer)UnresolvedStaticCall_159,
	(const Il2CppMethodPointer)UnresolvedStaticCall_160,
	(const Il2CppMethodPointer)UnresolvedStaticCall_161,
	(const Il2CppMethodPointer)UnresolvedStaticCall_162,
	(const Il2CppMethodPointer)UnresolvedStaticCall_163,
	(const Il2CppMethodPointer)UnresolvedStaticCall_164,
	(const Il2CppMethodPointer)UnresolvedStaticCall_165,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_170,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_176,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_181,
	(const Il2CppMethodPointer)UnresolvedStaticCall_182,
	(const Il2CppMethodPointer)UnresolvedStaticCall_183,
	(const Il2CppMethodPointer)UnresolvedStaticCall_184,
	(const Il2CppMethodPointer)UnresolvedStaticCall_185,
	(const Il2CppMethodPointer)UnresolvedStaticCall_186,
	(const Il2CppMethodPointer)UnresolvedStaticCall_187,
	(const Il2CppMethodPointer)UnresolvedStaticCall_188,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_191,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_193,
	(const Il2CppMethodPointer)UnresolvedStaticCall_194,
	(const Il2CppMethodPointer)UnresolvedStaticCall_195,
	(const Il2CppMethodPointer)UnresolvedStaticCall_196,
	(const Il2CppMethodPointer)UnresolvedStaticCall_197,
	(const Il2CppMethodPointer)UnresolvedStaticCall_198,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_201,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_207,
	(const Il2CppMethodPointer)UnresolvedStaticCall_208,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_211,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_214,
	(const Il2CppMethodPointer)UnresolvedStaticCall_215,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_218,
	(const Il2CppMethodPointer)UnresolvedStaticCall_219,
	(const Il2CppMethodPointer)UnresolvedStaticCall_220,
	(const Il2CppMethodPointer)UnresolvedStaticCall_221,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_223,
	(const Il2CppMethodPointer)UnresolvedStaticCall_224,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_226,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_252,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_266,
	(const Il2CppMethodPointer)UnresolvedStaticCall_267,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_270,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_274,
	(const Il2CppMethodPointer)UnresolvedStaticCall_275,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_277,
	(const Il2CppMethodPointer)UnresolvedStaticCall_278,
	(const Il2CppMethodPointer)UnresolvedStaticCall_279,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_281,
	(const Il2CppMethodPointer)UnresolvedStaticCall_282,
	(const Il2CppMethodPointer)UnresolvedStaticCall_283,
	(const Il2CppMethodPointer)UnresolvedStaticCall_284,
	(const Il2CppMethodPointer)UnresolvedStaticCall_285,
	(const Il2CppMethodPointer)UnresolvedStaticCall_286,
	(const Il2CppMethodPointer)UnresolvedStaticCall_287,
	(const Il2CppMethodPointer)UnresolvedStaticCall_288,
	(const Il2CppMethodPointer)UnresolvedStaticCall_289,
	(const Il2CppMethodPointer)UnresolvedStaticCall_290,
	(const Il2CppMethodPointer)UnresolvedStaticCall_291,
	(const Il2CppMethodPointer)UnresolvedStaticCall_292,
	(const Il2CppMethodPointer)UnresolvedStaticCall_293,
	(const Il2CppMethodPointer)UnresolvedStaticCall_294,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_296,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_300,
	(const Il2CppMethodPointer)UnresolvedStaticCall_301,
	(const Il2CppMethodPointer)UnresolvedStaticCall_302,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_304,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_312,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_316,
	(const Il2CppMethodPointer)UnresolvedStaticCall_317,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_324,
	(const Il2CppMethodPointer)UnresolvedStaticCall_325,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_330,
	(const Il2CppMethodPointer)UnresolvedStaticCall_331,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_343,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_345,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_347,
	(const Il2CppMethodPointer)UnresolvedStaticCall_348,
	(const Il2CppMethodPointer)UnresolvedStaticCall_349,
	(const Il2CppMethodPointer)UnresolvedStaticCall_350,
	(const Il2CppMethodPointer)UnresolvedStaticCall_351,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_355,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_371,
	(const Il2CppMethodPointer)UnresolvedStaticCall_372,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_381,
	(const Il2CppMethodPointer)UnresolvedStaticCall_382,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_386,
	(const Il2CppMethodPointer)UnresolvedStaticCall_387,
	(const Il2CppMethodPointer)UnresolvedStaticCall_388,
	(const Il2CppMethodPointer)UnresolvedStaticCall_389,
	(const Il2CppMethodPointer)UnresolvedStaticCall_390,
	(const Il2CppMethodPointer)UnresolvedStaticCall_391,
	(const Il2CppMethodPointer)UnresolvedStaticCall_392,
	(const Il2CppMethodPointer)UnresolvedStaticCall_393,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_396,
	(const Il2CppMethodPointer)UnresolvedStaticCall_397,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_409,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_430,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_434,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_438,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_455,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_463,
	(const Il2CppMethodPointer)UnresolvedStaticCall_464,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_471,
	(const Il2CppMethodPointer)UnresolvedStaticCall_472,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_474,
	(const Il2CppMethodPointer)UnresolvedStaticCall_475,
	(const Il2CppMethodPointer)UnresolvedStaticCall_476,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_478,
	(const Il2CppMethodPointer)UnresolvedStaticCall_479,
	(const Il2CppMethodPointer)UnresolvedStaticCall_480,
	(const Il2CppMethodPointer)UnresolvedStaticCall_481,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_486,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_488,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_490,
	(const Il2CppMethodPointer)UnresolvedStaticCall_491,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_493,
	(const Il2CppMethodPointer)UnresolvedStaticCall_494,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_497,
	(const Il2CppMethodPointer)UnresolvedStaticCall_498,
	(const Il2CppMethodPointer)UnresolvedStaticCall_499,
	(const Il2CppMethodPointer)UnresolvedStaticCall_500,
	(const Il2CppMethodPointer)UnresolvedStaticCall_501,
	(const Il2CppMethodPointer)UnresolvedStaticCall_502,
	(const Il2CppMethodPointer)UnresolvedStaticCall_503,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_505,
	(const Il2CppMethodPointer)UnresolvedStaticCall_506,
	(const Il2CppMethodPointer)UnresolvedStaticCall_507,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_511,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_513,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_515,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_517,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_520,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_522,
	(const Il2CppMethodPointer)UnresolvedStaticCall_523,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_525,
	(const Il2CppMethodPointer)UnresolvedStaticCall_526,
	(const Il2CppMethodPointer)UnresolvedStaticCall_527,
	(const Il2CppMethodPointer)UnresolvedStaticCall_528,
	(const Il2CppMethodPointer)UnresolvedStaticCall_529,
	(const Il2CppMethodPointer)UnresolvedStaticCall_530,
	(const Il2CppMethodPointer)UnresolvedStaticCall_531,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_533,
	(const Il2CppMethodPointer)UnresolvedStaticCall_534,
	(const Il2CppMethodPointer)UnresolvedStaticCall_535,
	(const Il2CppMethodPointer)UnresolvedStaticCall_536,
	(const Il2CppMethodPointer)UnresolvedStaticCall_537,
	(const Il2CppMethodPointer)UnresolvedStaticCall_538,
	(const Il2CppMethodPointer)UnresolvedStaticCall_539,
	(const Il2CppMethodPointer)UnresolvedStaticCall_540,
	(const Il2CppMethodPointer)UnresolvedStaticCall_541,
	(const Il2CppMethodPointer)UnresolvedStaticCall_542,
	(const Il2CppMethodPointer)UnresolvedStaticCall_543,
	(const Il2CppMethodPointer)UnresolvedStaticCall_544,
	(const Il2CppMethodPointer)UnresolvedStaticCall_545,
	(const Il2CppMethodPointer)UnresolvedStaticCall_546,
	(const Il2CppMethodPointer)UnresolvedStaticCall_547,
	(const Il2CppMethodPointer)UnresolvedStaticCall_548,
	(const Il2CppMethodPointer)UnresolvedStaticCall_549,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_551,
	(const Il2CppMethodPointer)UnresolvedStaticCall_552,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_554,
	(const Il2CppMethodPointer)UnresolvedStaticCall_555,
	(const Il2CppMethodPointer)UnresolvedStaticCall_556,
	(const Il2CppMethodPointer)UnresolvedStaticCall_557,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_559,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_561,
	(const Il2CppMethodPointer)UnresolvedStaticCall_562,
	(const Il2CppMethodPointer)UnresolvedStaticCall_563,
	(const Il2CppMethodPointer)UnresolvedStaticCall_564,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_569,
	(const Il2CppMethodPointer)UnresolvedStaticCall_570,
	(const Il2CppMethodPointer)UnresolvedStaticCall_571,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_589,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_603,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_624,
	(const Il2CppMethodPointer)UnresolvedStaticCall_625,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_628,
	(const Il2CppMethodPointer)UnresolvedStaticCall_629,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_631,
	(const Il2CppMethodPointer)UnresolvedStaticCall_632,
	(const Il2CppMethodPointer)UnresolvedStaticCall_633,
	(const Il2CppMethodPointer)UnresolvedStaticCall_634,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_636,
	(const Il2CppMethodPointer)UnresolvedStaticCall_637,
};
