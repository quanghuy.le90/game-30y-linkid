﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct InterfaceActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};

struct IUnityNativeSharing_tFDECD55F8CA3789FAEE9510380CCF89D5C9D43DD;
struct IUnityNativeSharingAdapter_tF5A011C75AB951074180ABCAFD9F4A5A01F8D225;
struct IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750;
struct String_t;
struct UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391;

IL2CPP_EXTERN_C RuntimeClass* IUnityNativeSharingAdapter_tF5A011C75AB951074180ABCAFD9F4A5A01F8D225_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391_il2cpp_TypeInfo_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct U3CModuleU3E_tDF36B473713433C01D15BCFFC330D1B2A1A95E6E 
{
};
struct IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750  : public RuntimeObject
{
};
struct String_t  : public RuntimeObject
{
	int32_t ____stringLength;
	Il2CppChar ____firstChar;
};
struct UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391  : public RuntimeObject
{
	RuntimeObject* ___adapter;
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	bool ___m_value;
};
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};
struct String_t_StaticFields
{
	String_t* ___Empty;
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	String_t* ___TrueString;
	String_t* ___FalseString;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif



IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareTextAndScreenshot_mAD7E41145E1F5687F9B6ED788D9A1F166B1A0763 (String_t* ___0_shareText, String_t* ___1_filePath, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareText_m48E027E0801C31B683596AEF30EB4F8E5CE8C5D2 (String_t* ___0_shareText, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IosUnityNativeSharingAdapter__ctor_m941379B5B49FDB2D8778B978234F62F4A8FEC786 (IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD (UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391* __this, RuntimeObject* ___0_adapter, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C void DEFAULT_CALL UnityNative_Sharing_ShareTextAndScreenshot(char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL UnityNative_Sharing_ShareText(char*);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareTextAndScreenshot_mAD7E41145E1F5687F9B6ED788D9A1F166B1A0763 (String_t* ___0_shareText, String_t* ___1_filePath, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	char* ____0_shareText_marshaled = NULL;
	____0_shareText_marshaled = il2cpp_codegen_marshal_string(___0_shareText);

	char* ____1_filePath_marshaled = NULL;
	____1_filePath_marshaled = il2cpp_codegen_marshal_string(___1_filePath);

	reinterpret_cast<PInvokeFunc>(UnityNative_Sharing_ShareTextAndScreenshot)(____0_shareText_marshaled, ____1_filePath_marshaled);

	il2cpp_codegen_marshal_free(____0_shareText_marshaled);
	____0_shareText_marshaled = NULL;

	il2cpp_codegen_marshal_free(____1_filePath_marshaled);
	____1_filePath_marshaled = NULL;

}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareText_m48E027E0801C31B683596AEF30EB4F8E5CE8C5D2 (String_t* ___0_shareText, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	char* ____0_shareText_marshaled = NULL;
	____0_shareText_marshaled = il2cpp_codegen_marshal_string(___0_shareText);

	reinterpret_cast<PInvokeFunc>(UnityNative_Sharing_ShareText)(____0_shareText_marshaled);

	il2cpp_codegen_marshal_free(____0_shareText_marshaled);
	____0_shareText_marshaled = NULL;

}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IosUnityNativeSharingAdapter_ShareScreenshotAndText_m689A7A38ADE318AF82C42FCAA7AEBEF92C89BBD1 (IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750* __this, String_t* ___0_shareText, String_t* ___1_filePath, bool ___2_showShareDialogBox, String_t* ___3_shareDialogBoxText, String_t* ___4_mimeType, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_shareText;
		String_t* L_1 = ___1_filePath;
		IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareTextAndScreenshot_mAD7E41145E1F5687F9B6ED788D9A1F166B1A0763(L_0, L_1, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IosUnityNativeSharingAdapter_ShareText_mA940BAB90566E2526E05E1DF1EBEAD51889505E5 (IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750* __this, String_t* ___0_shareText, bool ___1_showShareDialogBox, String_t* ___2_shareDialogBoxText, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_shareText;
		IosUnityNativeSharingAdapter_UnityNative_Sharing_ShareText_m48E027E0801C31B683596AEF30EB4F8E5CE8C5D2(L_0, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IosUnityNativeSharingAdapter__ctor_m941379B5B49FDB2D8778B978234F62F4A8FEC786 (IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UnityNativeSharing_Create_m659A1E5822976EFACAC96EA3C864748ACE462E13 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750* L_0 = (IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750*)il2cpp_codegen_object_new(IosUnityNativeSharingAdapter_t2ADEED78F99D93B49306DD3E22D95A3C8FA91750_il2cpp_TypeInfo_var);
		IosUnityNativeSharingAdapter__ctor_m941379B5B49FDB2D8778B978234F62F4A8FEC786(L_0, NULL);
		UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391* L_1 = (UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391*)il2cpp_codegen_object_new(UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391_il2cpp_TypeInfo_var);
		UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD(L_1, L_0, NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD (UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391* __this, RuntimeObject* ___0_adapter, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		RuntimeObject* L_0 = ___0_adapter;
		__this->___adapter = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___adapter), (void*)L_0);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityNativeSharing_ShareScreenshotAndText_m4D796344268E8048062152322841F90EFBE213CD (UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391* __this, String_t* ___0_shareText, String_t* ___1_filePath, bool ___2_showShareDialogBox, String_t* ___3_shareDialogBoxText, String_t* ___4_mimeType, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IUnityNativeSharingAdapter_tF5A011C75AB951074180ABCAFD9F4A5A01F8D225_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->___adapter;
		String_t* L_1 = ___0_shareText;
		String_t* L_2 = ___1_filePath;
		bool L_3 = ___2_showShareDialogBox;
		String_t* L_4 = ___3_shareDialogBoxText;
		String_t* L_5 = ___4_mimeType;
		NullCheck(L_0);
		InterfaceActionInvoker5< String_t*, String_t*, bool, String_t*, String_t* >::Invoke(0, IUnityNativeSharingAdapter_tF5A011C75AB951074180ABCAFD9F4A5A01F8D225_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3, L_4, L_5);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityNativeSharing_ShareText_m6BE537F2B56982778AC3B7ECA28CA488D64341A9 (UnityNativeSharing_tE42BA696B4CC30FED562290A4349A44FDCF3B391* __this, String_t* ___0_shareText, bool ___1_showShareDialogBox, String_t* ___2_shareDialogBoxText, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IUnityNativeSharingAdapter_tF5A011C75AB951074180ABCAFD9F4A5A01F8D225_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->___adapter;
		String_t* L_1 = ___0_shareText;
		bool L_2 = ___1_showShareDialogBox;
		String_t* L_3 = ___2_shareDialogBoxText;
		NullCheck(L_0);
		InterfaceActionInvoker3< String_t*, bool, String_t* >::Invoke(1, IUnityNativeSharingAdapter_tF5A011C75AB951074180ABCAFD9F4A5A01F8D225_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
