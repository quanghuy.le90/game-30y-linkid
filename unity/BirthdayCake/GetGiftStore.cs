using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class test : MonoBehaviour
{
	[SerializeField] GameObject NullGift;
	public struct Game
	{
		public string Name;
		public string eGiftCode;
	}

	[SerializeField] Image Icon;
	private void Start()
	{
		StartCoroutine("GetGiftImage");
		
	}

	void DrawUI()
    {
        GetUserGift allGames = APIs.GetUseGift();

        int N = allGames.results.Length;
       
		GameObject buttonTemplate = transform.GetChild(0).gameObject;
		GameObject g;
		for (int i = 0; i < N; i++)
		{
			g = Instantiate(buttonTemplate, transform);
			g.transform.GetChild(1).GetComponent<Text>().text = allGames.results[i].name;
		}
		
		Destroy(buttonTemplate);
	}
	IEnumerator GetGiftImage()
	{
		GetUserGift allGames = APIs.GetUseGift();

		if (allGames.results.Length == 0)
        {
			NullGift.SetActive(true);
		}
        else
        {

			for (int i = 0; i < allGames.results.Length; i++)
			{
				WWW w = new WWW(allGames.results[i].image);
				yield return w;

				if (w.error != null)
				{
					//NullGift.SetActive(true);
				}
				else
				{
					if (w.isDone)
					{

						Texture2D myTexture = w.texture;
						Sprite newSPrite = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f));

						Icon.sprite = newSPrite;

					}
				}
			}
			DrawUI();
		}

		
	}

}

	