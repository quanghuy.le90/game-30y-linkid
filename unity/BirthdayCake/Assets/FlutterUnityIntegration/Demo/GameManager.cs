﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] Button btnPause , btnExit;
    [SerializeField] GameObject PopPause , CandlePlay;

    public void Start()
    {
        btnPause.onClick.AddListener(() => PauseGame()); ;
        btnExit.onClick.AddListener(() => SceneManager.LoadScene("MainMenu"));
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
        PopPause.SetActive(true);

       


    }
}
