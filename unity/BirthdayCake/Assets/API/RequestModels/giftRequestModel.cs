using Newtonsoft.Json;

namespace BirthdayCake.APIs.RequestModels
{
    public struct HaveGift
    {
        [JsonProperty("image")] public string image;
        [JsonProperty("name")] public string name;
        [JsonProperty("egiftCode")] public string egiftCode; 
      
    }
}
