using Newtonsoft.Json;

namespace BirthdayCake.APIs.RequestModels
{
    public struct Share
    {
        [JsonProperty("message")] public string message;
    }
}
