using Newtonsoft.Json;

namespace BirthdayCake.APIs.RequestModels
{
    public struct PlayGame
    {
        [JsonProperty("resultPlay")] public string resultPlay;
        [JsonProperty("isWin")] public int isWin;
        [JsonProperty("playTime")] public string playTime;
    }
}
