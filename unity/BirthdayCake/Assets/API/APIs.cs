﻿using UnityEngine;
using System.Net;
using System.IO;
using BirthdayCake.APIs.RequestModels;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using UnityEditor;

public static class APIs
{

    //private static string tokenTest = "eenL+f48pW3do5qiF+kIg604b6kZ7TXKb2wbd31m7IN2b1uaGNUDXorAivJ1o6Rnu6Vmyk+5kks63ux0MUkeMUJf1iStoqQGNZkzOhgJTciM9z9FmitCB8Pc";
    public static string token;
    public static void SetToken(string tokenLinkiD)
    {
        token = tokenLinkiD;

    }

    //public static GenKey GenKey()
    //{
    //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("http://k8s-gamelink-gamelink-a94bf3d872-1704347399.ap-southeast-1.elb.amazonaws.com/game-linkid30-be/api/v1/game-play/gen-key?userCode=uHD8T3PGFYckCvkETB54UnJzspf2&userName=manhnh"));
    //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //    StreamReader reader = new StreamReader(response.GetResponseStream());
    //    string token = reader.ReadToEnd();
    //    reader.Close();



    //    GenKey genKey = JsonUtility.FromJson<GenKey>(token);
    //    return genKey;
    //}
    //public static NumberOfPlay GetNewData()
    //{

    //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("http://k8s-gamelink-gamelink-a94bf3d872-1704347399.ap-southeast-1.elb.amazonaws.com/game-linkid30-be/api/v1/game-play/number-of-play"));
    //    request.Headers.Add("Authorization", $"Bearer {token}");

    //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //    StreamReader reader = new StreamReader(response.GetResponseStream());
    //    string Data = reader.ReadToEnd();
    //    reader.Close();

    //    JObject joResponse = JObject.Parse(Data);
    //    JObject ojObject = (JObject)joResponse["result"];
    //    string data = ojObject.ToString();

    //    NumberOfPlay numberOfPlay = JsonUtility.FromJson<NumberOfPlay>(data);
    //    return numberOfPlay;
    //}

    //public static InitGameResponse Init()
    //{
    //    try
    //    {
    //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("http://k8s-gamelink-gamelink-a94bf3d872-1704347399.ap-southeast-1.elb.amazonaws.com/game-linkid30-be/api/v1/game-play/init"));
    //        request.Headers.Add("Authorization", $"Bearer {token}");

    //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //        StreamReader reader = new StreamReader(response.GetResponseStream());

    //        string initData = reader.ReadToEnd();
    //        reader.Close();
    //        JObject joResponse = JObject.Parse(initData);
    //        JObject ojObject = (JObject)joResponse["result"];
    //        JObject ojObject1 = (JObject)ojObject["userInfo"];
    //        string dataInit = ojObject1.ToString();
    //        InitGameResponse init = JsonUtility.FromJson<InitGameResponse>(dataInit);
    //        return init;

    //    }
    //    catch (Exception e)
    //    {
    //        string initData = e.Message;
    //        InitGameResponse init = JsonUtility.FromJson<InitGameResponse>(initData);
    //        throw;
    //    }
    //}

    //public static GameInfo GameInfo()
    //{
    //    try
    //    {
    //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("http://k8s-gamelink-gamelink-a94bf3d872-1704347399.ap-southeast-1.elb.amazonaws.com/game-linkid30-be/api/v1/game-play/init"));
    //        request.Headers.Add("Authorization", $"Bearer {token}");

    //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //        StreamReader reader = new StreamReader(response.GetResponseStream());

    //        string initData = reader.ReadToEnd();
    //        reader.Close();
    //        JObject joResponse = JObject.Parse(initData);
    //        JObject ojObject = (JObject)joResponse["result"];
    //        JObject ojObject1 = (JObject)ojObject["gameInfo"];
    //        string dataInit = ojObject1.ToString();
    //        GameInfo init = JsonUtility.FromJson<GameInfo>(dataInit);
    //        return init;

    //    }
    //    catch (Exception e)
    //    {
    //        string initData = e.Message;
    //        GameInfo init = JsonUtility.FromJson<GameInfo>(initData);
    //        return init;
    //    }
    //}
    //public static PlayGame PlayGame()
    //{
    //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("https://vpb30-linkid.gami.linkid.vn/game-linkid30-be/api/v1/game-play/play"));
    //    request.Headers.Add("Authorization", $"Bearer {token}");

    //    request.Method = "POST";

    //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //    StreamReader reader = new StreamReader(response.GetResponseStream());
    //    string result = reader.ReadToEnd();
    //    reader.Close();

    //    JObject joResponse = JObject.Parse(result);
    //    JObject ojObject = (JObject)joResponse["result"];
    //    string dataResult = ojObject.ToString();

    //    PlayGame resultGame = JsonUtility.FromJson<PlayGame>(dataResult);
    //    return resultGame;
    //}

    //public static HaveGift Gift(string resultPlay, string time)
    //{
    //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("http://k8s-gamelink-gamelink-a94bf3d872-1704347399.ap-southeast-1.elb.amazonaws.com/game-linkid30-be/api/v1/game-play/gift-transfer"));
    //    request.Headers.Add("Authorization", $"Bearer {token}");

    //    request.Method = "POST";
    //    request.ContentType = "application/json";
    //    try
    //    {
    //        using (var streamWriter = new StreamWriter(request.GetRequestStream()))
    //        {
    //            string jsonString = JsonConvert.SerializeObject(new { playResult = resultPlay, playTime = time });
    //            streamWriter.Write(jsonString);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Debug.LogError(ex);
    //    }


    //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //    StreamReader reader = new StreamReader(response.GetResponseStream());
    //    string gift = reader.ReadToEnd();

    //    reader.Close();
    //    JObject joResponse = JObject.Parse(gift);
    //    JObject ojObject = (JObject)joResponse["result"];
    //    string data = ojObject.ToString();
    //    HaveGift gifttGame = JsonUtility.FromJson<HaveGift>(data);

    //    return gifttGame;
    //}

    public static GetUserGift GetUseGift()
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("https://vpb30-linkid.gami.linkid.vn/game-linkid30-be/api/v1/game-play/user-gifts"));
        request.Headers.Add("Authorization", $"Bearer {token}");

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());

        string arrayData = reader.ReadToEnd();
        reader.Close();
        JObject joResponse = JObject.Parse(arrayData);

        GetUserGift result = JsonConvert.DeserializeObject<GetUserGift>(joResponse.ToString());
        //Debug.Log("Result Length: " + result.results.Length);
        return result;
    }

    //public static Share Share()
    //{
    //    HttpWebRequest request = (HttpWebRequest)WebRequest.
    //    Create(string.Format("http://k8s-gamelink-gamelink-a94bf3d872-1704347399.ap-southeast-1.elb.amazonaws.com/game-linkid30-be/api/v1/game-play/share"));

    //    request.Headers.Add("Authorization", $"Bearer {token}");

    //    request.Method = "POST";

    //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //    StreamReader reader = new StreamReader(response.GetResponseStream());
    //    string gift = reader.ReadToEnd();
    //    reader.Close();
    //    JObject joResponse = JObject.Parse(gift);
    //    JObject ojObject = (JObject)joResponse["message"];
    //    string data = ojObject.ToString();
    //    Share share = JsonUtility.FromJson<Share>(data);
    //    return share;
    //}

    //public static GetCloseGame CloseGame()
    //{
    //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("http://k8s-gamelink-gamelink-a94bf3d872-1704347399.ap-southeast-1.elb.amazonaws.com/game-linkid30-be/api/v1/game-play/close-game"));
    //    request.Headers.Add("Authorization", $"Bearer {token}");

    //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    //    StreamReader reader = new StreamReader(response.GetResponseStream());
    //    string Data = reader.ReadToEnd();
    //    reader.Close();

    //    GetCloseGame close = JsonUtility.FromJson<GetCloseGame>(Data);
    //    return close;
    //}
}


