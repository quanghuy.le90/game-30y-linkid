
using Newtonsoft.Json;

[System.Serializable]
public struct GameInfo
{
    [JsonProperty("startDate")] public string startDate;

    [JsonProperty("endDate")] public string endDate;

    [JsonProperty("maxNoPlayPlusPerDay")] public int maxNoPlayPlusPerDay;

    [JsonProperty("version")] public string version;

   
}