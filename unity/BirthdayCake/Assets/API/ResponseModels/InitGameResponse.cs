
using Newtonsoft.Json;

[System.Serializable]
public struct InitGameResponse
{
            [JsonProperty("userName")] public string userName;

            [JsonProperty("userCode")] public string userCode;

            [JsonProperty("phoneNumber")] public int phoneNumber;

            [JsonProperty("numberOfPlay")] public int numberOfPlay;

            [JsonProperty("maxNoPlayPlusPerDay")] public int maxNoPlayPlusPerDay;

            [JsonProperty("level")] public int level;

            [JsonProperty("numberShareInDay")] public int numberShareInDay;
}

    



