using Newtonsoft.Json;

[System.Serializable]

public struct GetCloseGame
{

    [JsonProperty("message")]
    public string message;

}
