using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Purchasing;

[System.Serializable]
public struct NumberOfPlay
{
   
        [JsonProperty("id")] public int id;

        [JsonProperty("userName")] public string userName;

        [JsonProperty("userCode")] public string userCode;

        [JsonProperty("email")] public string email;

        [JsonProperty("phoneNumber")] public int phoneNumber;

        [JsonProperty("numberOfPlays")] public int numberOfPlays;

        [JsonProperty("level")] public int level;

}

  


