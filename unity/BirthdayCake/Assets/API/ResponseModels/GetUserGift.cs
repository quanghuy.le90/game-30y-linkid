using Newtonsoft.Json;
using System.Collections.Generic;

[System.Serializable]
public struct GetUserGift
{
    [JsonProperty("result")] public result[] results;

}

public struct result
{
    [JsonProperty("eGiftCode")] public string eGiftCode;
    [JsonProperty("name")] public string name;
    [JsonProperty("image")] public string image;
}

public class UserGift {
    public Gifts[] result { get; set; }
    public string message { get; set; }
}

public class Gifts {
    public string name { get; set; }
    public string eGiftCode { get; set; }
    public string image { get; set; }
}