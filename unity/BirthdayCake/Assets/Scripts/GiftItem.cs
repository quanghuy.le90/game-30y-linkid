using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GiftItem : MonoBehaviour
{
    [SerializeField]
    private Image giftImage;
    [SerializeField]
    private Text giftName;

    private Gifts giftData;

    public void InitGiftItem(Gifts gift) {
        giftName.text = gift.name;
        giftData = gift;
        StartCoroutine(nameof(GetGiftImage));
    }

    IEnumerator GetGiftImage()
    {
        UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(giftData.image);

        yield return webRequest.SendWebRequest();
        switch (webRequest.result)
        {
            case UnityWebRequest.Result.InProgress:
                Debug.LogError("Inprogress");
                break;

            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.ProtocolError:
            case UnityWebRequest.Result.DataProcessingError:
                Debug.LogError("Error");
                break;

            case UnityWebRequest.Result.Success:
                Texture2D myTexture = ((DownloadHandlerTexture)webRequest.downloadHandler).texture;
                Sprite image = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f));
                giftImage.sprite = image;
                break;
        }
    }
}
