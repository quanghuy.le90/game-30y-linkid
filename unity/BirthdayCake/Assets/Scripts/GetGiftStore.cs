using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

public class GetGiftStore : MonoBehaviour
{
    [SerializeField] GameObject NullGift;
    public struct Game
    {
        public string Name;
        public string eGiftCode;
    }

    [SerializeField] Image Icon;

    private readonly List<GameObject> giftList = new();
    [SerializeField]
    private GameObject giftPrf;

    private void Start()
    {
        //StartCoroutine("GetGiftImage");
        InitGiftStorage();
    }

    private void InitGiftStorage()
    {
        int N = 0;
        ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.giftUser, null, APIs.token, (result) =>
        {
            ////TODO remove late
            //TextAsset targetFile = Resources.Load<TextAsset>("Data/gift");
            //result = targetFile.text;
            ////End

            JObject resultObj = JObject.Parse(result);
            if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
            {
                try
                {
                    NullGift.SetActive(false);
                    if (giftList.Count > 0)
                    {
                        for (int i = 0; i <= giftList.Count; i++)
                        {
                            giftList[i].SetActive(false);
                        }
                    }

                    UserGift userGift = JsonConvert.DeserializeObject<UserGift>(resultObj.ToString());

                    N = userGift.result.Length;
                    if (userGift.result.Length <= giftList.Count)
                    {
                        for (int i = 0; i < userGift.result.Length; i++)
                        {
                            giftList[i].SetActive(true);
                            giftList[i].GetComponent<GiftItem>().InitGiftItem(userGift.result[i]);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < userGift.result.Length; i++)
                        {
                            GameObject gift = Instantiate(giftPrf, transform);
                            gift.GetComponent<GiftItem>().InitGiftItem(userGift.result[i]);
                            giftList.Add(gift);
                        }
                    }
                }
                catch (Exception e)
                {
                    NullGift.SetActive(true);
                    Debug.Log("Exception: " + e.Message);
                    GameObject buttonTemplate = transform.GetChild(0).gameObject;
                    Destroy(buttonTemplate);
                }
            }
            else
            {
                Debug.LogError("Error: " + resultObj["message"]);
            }
        });
    }

    //void DrawUI()
    //{
    //    //GetUserGift allGames = APIs.GetUseGift();

    //    int N = 0;
    //    ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.giftUser, null, APIs.token, (result) =>
    //    {
    //        JObject resultObj = JObject.Parse(result);
    //        if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
    //        {
    //            Debug.Log("response: " + resultObj["result"].ToString());
    //            try
    //            {
    //                GetUserGift getUserGift = JsonConvert.DeserializeObject<GetUserGift>(resultObj["result"].ToString());
    //                N = getUserGift.results.Length;
    //                GameObject buttonTemplate = transform.GetChild(0).gameObject;
    //                GameObject g;
    //                for (int i = 0; i < N; i++)
    //                {
    //                    g = Instantiate(buttonTemplate, transform);
    //                    g.transform.GetChild(1).GetComponent<Text>().text = getUserGift.results[i].name;
    //                }
    //                Destroy(buttonTemplate);
    //            }
    //            catch (Exception e)
    //            {
    //                Debug.Log("Exception: " + e.Message);
    //                GameObject buttonTemplate = transform.GetChild(0).gameObject;
    //                Destroy(buttonTemplate);
    //            }
    //        }
    //        else
    //        {
    //            Debug.LogError("Error: " + resultObj["message"]);
    //        }
    //    });
    //}

    //IEnumerator GetGiftImage()
    //{
    //    GetUserGift allGames = APIs.GetUseGift();

    //    if (allGames.results.Length == 0)
    //    {
    //        NullGift.SetActive(true);
    //    }
    //    else
    //    {

    //        for (int i = 0; i < allGames.results.Length; i++)
    //        {

    //            UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(allGames.results[i].image);

    //            yield return webRequest.SendWebRequest();
    //            switch (webRequest.result)
    //            {
    //                case UnityWebRequest.Result.InProgress:
    //                    Debug.LogError("Inprogress");
    //                    break;

    //                case UnityWebRequest.Result.ConnectionError:
    //                case UnityWebRequest.Result.ProtocolError:
    //                case UnityWebRequest.Result.DataProcessingError:
    //                    Debug.LogError("Error");
    //                    break;

    //                case UnityWebRequest.Result.Success:

    //                    Texture2D myTexture = ((DownloadHandlerTexture)webRequest.downloadHandler).texture;
    //                    Sprite image = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f));
    //                    Icon.sprite = image;
    //                    break;


    //            }
    //        }
    //    }
    //    DrawUI();
    //}


}



