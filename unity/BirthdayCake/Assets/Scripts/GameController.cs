﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BirthdayCake.APIs.RequestModels;
using FlutterUnityIntegration;
using UnityEngine.Networking;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using static Cinemachine.DocumentationSortingAttribute;

[RequireComponent(typeof(GameUI))]
public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    [SerializeField] GameObject goad1, goad2, goad3, goad1Hard, goad2Hard, goad3Hard;

    [SerializeField] Button btnBack, btnReturn, btnShare, btnNext, btnCloseGamePlay, btnUseNow;
    [SerializeField] private int candleCount;

    [Header("Candle Spawning")]
    [SerializeField] private Vector2 candleSpawnPosition;

    [SerializeField] private GameObject candleObject;

    [SerializeField]
    public GameObject cake, PopupWin, PopupLost, PopupRule, PopupChampion, PopupShare, btn_back, btn_return, btn_share, Popuplv3Win, errorDialog;
    public Text CodeGift, countNumberOfPlay;

    [SerializeField] Image Icon;

    public float slow = 10f;
    public string CodeResult, PlayTime;
    public int result;
    public GameUI GameUI { get; private set; }
    public int checklevel, _numberOfPlay;

    private void Awake()
    {
        Debug.Log(APIs.token);
        Instance = this;
        GameUI = GetComponent<GameUI>();
    }

    void Start()
    {
        Debug.Log("Start GameController");
        ApiHelper.Instance.RequestWebService(APIMethod.POST.ToString(), Endpoints.playGame, null, APIs.token, (resultPlayGame) => {
            JObject resultObj = JObject.Parse(resultPlayGame);
            if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
            {
                Debug.Log("playGame: " + resultObj["result"].ToString());

                PlayGame playGame = JsonConvert.DeserializeObject<PlayGame>(resultObj["result"].ToString());
                CodeResult = playGame.resultPlay;
                result = playGame.isWin;
                PlayTime = playGame.playTime;
                checkNumberCandle();
            }
            else
            {
                Debug.LogError(resultObj["message"]);
                errorDialog.SetActive(true);
                errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                {
                    Debug.Log("Quit");
                    errorDialog.SetActive(false);
                }, () =>
                {
                    // InitGame();
                    errorDialog.SetActive(false);
                });

            }
        });

        

    }

    public void checkNumberCandle()
    {
        //InitGameResponse i = APIs.Init();ˆ

        ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.initGame, null, APIs.token, (result)=> {
            JObject resultObj = JObject.Parse(result);
            if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
            {
                InitGameResponse initGameResponse = JsonConvert.DeserializeObject<InitGameResponse>(resultObj["result"]["userInfo"].ToString());

                checklevel = initGameResponse.level;
                _numberOfPlay = initGameResponse.numberOfPlay;

                if (checklevel == 1)
                {
                    candleCount = 12;

                }
                else if (checklevel == 2)
                {
                    candleCount = 9;
                }
                else if (checklevel == 3)
                {
                    candleCount = 6;
                }

                CheckResult(checklevel);

                GameUI.SetInitialDisplayCandleCount(candleCount);
                SpawnCandle();
            }
            else
            {
                Debug.LogError(resultObj["message"]);
                errorDialog.SetActive(true);
                errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                {
                    Debug.Log("Quit");
                    errorDialog.SetActive(false);
                }, () =>
                {
                    // InitGame();
                    errorDialog.SetActive(false);
                });

            }
        });

        
    }

    public void CheckResult(int level)
    {
        if (level == 1)
        {
            if (result == 0)
            {
                goad1Hard.SetActive(true);
            }
            else
            {
                goad1.SetActive(true);
            }
        }
        else if (level == 2)
        {
            if (result == 0)
            {
                goad2Hard.SetActive(true);
            }
            else
            {
                goad2.SetActive(true);
            }
        }
        else
        {
            if (result == 0)
            {
                goad3Hard.SetActive(true);
            }
            else
            {
                goad3.SetActive(true);
            }
        }


    }
    public void OnSuccesfulCandleHit()
    {
        if (candleCount > 0)
        {
            SpawnCandle();
        }
        else
        {
            StartGameOverSequence(true);
        }
    }

    private void SpawnCandle()
    {
        Debug.Log("SpawnCandle");
        candleCount--;
        GameObject candle = Instantiate(candleObject, candleSpawnPosition, Quaternion.identity);
        candle.GetComponent<CandleScrips>().InitCandle(this);
    }

    public void StartGameOverSequence(bool win)
    {
        StartCoroutine("GameOverSequenceCoroutine", win);

    }

    private IEnumerator GameOverSequenceCoroutine(bool win)
    {
        if (result == 1)
        {
            if (win)
            {
                yield return new WaitForSecondsRealtime(2.5f);
                if (checklevel == 3)
                {
                    Popuplv3Win.SetActive(true);
                    cake.SetActive(false);

                    string jsonString = JsonConvert.SerializeObject(new { playResult = CodeResult, playTime = PlayTime });

                    ApiHelper.Instance.RequestWebService(APIMethod.POST.ToString(), Endpoints.giftTranfer, jsonString, APIs.token, (result) =>
                    {
                        try
                        {
                            JObject resultObj = JObject.Parse(result);
                            if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
                            {

                                HaveGift haveGift = JsonConvert.DeserializeObject<HaveGift>(resultObj["result"].ToString());

                            }
                            else
                            {
                                Debug.LogError(resultObj["message"]);
                                errorDialog.SetActive(true);
                                errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                                {
                                    Debug.Log("Quit");
                                    errorDialog.SetActive(false);
                                }, () =>
                                {
                                    // InitGame();
                                    errorDialog.SetActive(false);
                                });

                            }
                        }
                        catch (System.Exception e)
                        {
                            Debug.LogError("Error result: " + result);
                            Debug.LogError("Error: " + e.Message);
                        }
                    });
                    AudioManager.Instance.PlaySFX("Win");
                    btnCloseGamePlay.onClick.AddListener(() => SceneManager.LoadScene("MainMenu"));
                    btnUseNow.onClick.AddListener(() => GetComponent<UnityMessageManager>().SendMessageToFlutter("linkid"));

                }
                else
                {
                    string jsonString = JsonConvert.SerializeObject(new { playResult = CodeResult, playTime = PlayTime });

                    ApiHelper.Instance.RequestWebService(APIMethod.POST.ToString(), Endpoints.giftTranfer, jsonString, APIs.token, (result) =>
                    {
                        try
                        {
                            JObject resultObj = JObject.Parse(result);
                            if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
                            {

                                HaveGift haveGift = JsonConvert.DeserializeObject<HaveGift>(resultObj["result"].ToString());

                                CodeGift.text = haveGift.name;
                                string url = haveGift.image;

                                StartCoroutine(setImage(url));

                            }
                            else
                            {
                                Debug.LogError(resultObj["message"]);
                                errorDialog.SetActive(true);
                                errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                                {
                                    Debug.Log("Quit");
                                    errorDialog.SetActive(false);
                                }, () =>
                                {
                                    // InitGame();
                                    errorDialog.SetActive(false);
                                });

                            }
                        }
                        catch (System.Exception e)
                        {
                            Debug.LogError("Error result: " + result);
                            Debug.LogError("Error: " + e.Message);
                        }
                    });





                    PopupWin.SetActive(true);
                    btnNext.onClick.AddListener(() => RestartGame());
                    cake.SetActive(false);

                    AudioManager.Instance.PlaySFX("Win");

                }
            }
            else
            {
                StartCoroutine(Slow());

                string data = "a_lose";

                string jsonString = JsonConvert.SerializeObject(new { playResult = data, playTime = PlayTime });

                ApiHelper.Instance.RequestWebService(APIMethod.POST.ToString(), Endpoints.giftTranfer, jsonString, APIs.token, (result) =>
                {
                    try
                    {
                        JObject resultObj = JObject.Parse(result);
                        if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
                        {

                            HaveGift haveGift = JsonConvert.DeserializeObject<HaveGift>(resultObj["result"].ToString());

                        }
                        else
                        {
                            Debug.LogError(resultObj["message"]);
                            errorDialog.SetActive(true);
                            errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                            {
                                Debug.Log("Quit");
                                errorDialog.SetActive(false);
                            }, () =>
                            {
                                // InitGame();
                                errorDialog.SetActive(false);
                            });

                        }
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError("Error result: " + result);
                        Debug.LogError("Error: " + e.Message);
                    }
                });

                yield return new WaitForSecondsRealtime(2.5f);
                ShowPopupLose();
                AudioManager.Instance.PlaySFX("Fail");

            }
        }
        else
        {

            if (win)
            {

                errorDialog.SetActive(true);

            }
            else
            {


                string jsonString = JsonConvert.SerializeObject(new { playResult = CodeResult, playTime = PlayTime });

                ApiHelper.Instance.RequestWebService(APIMethod.POST.ToString(), Endpoints.giftTranfer, jsonString, APIs.token, (result) =>
                {
                    try
                    {
                        JObject resultObj = JObject.Parse(result);
                        if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
                        {

                            HaveGift haveGift = JsonConvert.DeserializeObject<HaveGift>(resultObj["result"].ToString());

                        }
                        else
                        {
                            Debug.LogError(resultObj["message"]);
                            errorDialog.SetActive(true);
                            errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                            {
                                Debug.Log("Quit");
                                errorDialog.SetActive(false);
                            }, () =>
                            {
                                // InitGame();
                                errorDialog.SetActive(false);
                            });

                        }
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError("Error result: " + result);
                        Debug.LogError("Error: " + e.Message);
                    }
                });

                ShowPopupLose();
                AudioManager.Instance.PlaySFX("Fail");

            }
        }
    }

    IEnumerator setImage(string url)
    {
        UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(url);

        yield return webRequest.SendWebRequest();
        switch (webRequest.result)
        {
            case UnityWebRequest.Result.InProgress:
                Debug.LogError("Inprogress");
                break;

            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.ProtocolError:
            case UnityWebRequest.Result.DataProcessingError:
                Debug.LogError("Error");
                break;

            case UnityWebRequest.Result.Success:

                Texture2D myTexture = ((DownloadHandlerTexture)webRequest.downloadHandler).texture;
                Sprite image = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f));
                Icon.sprite = image;
                break;


        }

    }
    IEnumerator Slow()
    {
        Time.timeScale = 1f / slow;
        Time.fixedDeltaTime /= slow;
        yield return new WaitForSeconds(2f / slow);
        Time.timeScale = 1f;
        Time.fixedDeltaTime *= slow;
    }
    public void RestartGame()
    {
        Debug.Log("RestartGame");
        SceneManager.LoadScene("GamePlay");
    }
    private void ShowPopupLose()
    {

        cake.SetActive(false);
        PopupLost.SetActive(true);

        int numberOfPlay = _numberOfPlay;
        countNumberOfPlay.text = string.Format("Số lượt chơi còn lại : <b>{0}</b> ", numberOfPlay);

        if (numberOfPlay > 0)
        {
            btn_back.SetActive(true);
            btn_return.SetActive(true);
            btnReturn.onClick.AddListener(() => RestartGame());
            btnBack.onClick.AddListener(() => SceneManager.LoadScene("MainMenu"));

        }
        else
        {
            btn_back.SetActive(true);
            btn_share.SetActive(true);
            btnBack.onClick.AddListener(() => SceneManager.LoadScene("MainMenu"));
            btnShare.onClick.AddListener(() => ShowPopupShare());
        }

    }

    private void ShowPopupShare()
    {

        SceneManager.LoadScene("MainMenu");
    }

    public bool isPause = false;
    public void PauseGame()
    {
        Debug.Log("isPause:" + isPause);
        if (isPause)
        {
            Time.timeScale = 1;
            isPause = false;
        }
        else
        {
            Time.timeScale = 0;
            isPause = true;
        }
    }

    public void BackToMain()
    {
        SceneManager.LoadSceneAsync(Constant.MainScene);
    }

    public void BackToLinkStorage() {
        GetComponent<UnityMessageManager>().SendMessageToFlutter("linkid");
    }

    private void OnApplicationQuit()
    {
        ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.closeGame, null, APIs.token, (result) =>
        {
            try
            {
                JObject resultObj = JObject.Parse(result);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Error: " + e.Message);
            }
        });
    }
}