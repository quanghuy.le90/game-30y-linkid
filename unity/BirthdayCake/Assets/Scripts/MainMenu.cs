﻿
using FlutterUnityIntegration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour

{
    [SerializeField] Button btnStartGame, btn_info, btn_share, btn_gift, btn_setting, btn_out, btn_share1;
    [SerializeField] Text textNumberOfPlay, title, texMaxNoPlayPlusPerDay;
    [SerializeField] GameObject PopupEnd, PopupShare, PopoupChamp, PopupError, errorDialog;

    private int numberOfPlay, maxNoPlayPlusPerDay, numberShareInDay;

    public int level;

    public Texture2D shareImage = null;
    public void GetToken(string message)
    {
        APIs.SetToken(message);

        GetComponent<UnityMessageManager>().SendMessageToFlutter("unityloaded");

    }

    void Start()
    {
        Debug.LogWarning("Start Unity Game");
        if (shareImage == null)
        {
            shareImage = Resources.Load<Texture2D>("Images/share_image");
        }

        if (!string.IsNullOrEmpty(APIs.token))
        {
            Debug.Log("token: " + APIs.token);
            InitGame();
        }
        else
        {
#if UNITY_EDITOR

            Debug.LogWarning("Editor" + APIs.token);
            APIs.SetToken("Ua69wsRBmEfd25ySFsYrtY9HC79g3hTLfXADdn1m7IN2b1uaGNUDXorAivJ1o6Rnu6Vmyk+5kks63ux0MUkeMUJf1iSto6QFNZmktGUK8B1XUiAtir36+HdE");

            InitGame();

#endif
        }

        EvenButtonTop();

#if UNITY_IPHONE
        title.text = "Apple không tài trợ cho chương trình này";
#elif UNITY_ANDROID
        title.text = "Google không tài trợ cho chương trình này";
#endif

        Button _btnStartGame = btnStartGame.GetComponent<Button>();
        _btnStartGame.onClick.AddListener(StartGame);

        void StartGame()
        {
            CheckNumberOfPlay();
        }
    }

    private void InitGame()
    {
        ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.initGame, null, APIs.token, (result) =>
        {
            try
            {
                JObject resultObj = JObject.Parse(result);
                if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
                {


                    GameInfo gameInfo = JsonConvert.DeserializeObject<GameInfo>(resultObj["result"]["gameInfo"].ToString());
                    InitGameResponse initGameResponse = JsonConvert.DeserializeObject<InitGameResponse>(resultObj["result"]["userInfo"].ToString());
                    textNumberOfPlay.text = string.Format("<b>{0}</b>", initGameResponse.numberOfPlay);

                    numberOfPlay = initGameResponse.numberOfPlay;
                    numberShareInDay = initGameResponse.numberShareInDay;
                    maxNoPlayPlusPerDay = gameInfo.maxNoPlayPlusPerDay;
                    level = initGameResponse.level;
                }
                else
                {
                    Debug.LogError(resultObj["message"]);
                    errorDialog.SetActive(true);
                    errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                    {
                        Debug.Log("Quit");
                        errorDialog.SetActive(false);
                    }, () => {
                        InitGame();
                        errorDialog.SetActive(false);
                    });

                }
            }
            catch (System.Exception e)
            {
                errorDialog.SetActive(true);

                Debug.LogError("Error result: " + result);
                Debug.LogError("Error: " + e.Message);
            }
        });
    }

    private void CheckNumberOfPlay()
    {
        if (level > 3)
        {
            PopoupChamp.SetActive(true);
        }
        else
        {
            if (numberOfPlay > 0)
            {
                SceneManager.LoadScene("GamePlay");

            }
            else
            {
                ChecKShareEveryDay();
            }

        }

    }

    public void RequestCallback() { }

    public void ChecKShareEveryDay()
    {

        if (numberShareInDay > maxNoPlayPlusPerDay)
        {

            PopupEnd.SetActive(true);
            texMaxNoPlayPlusPerDay.text = string.Format("<b>{0}</b> <b>lượt chơi/ngày</b>", maxNoPlayPlusPerDay);
        }
        else
        {
            ShowPopupShare();
        }
    }

    private void EvenButtonTop()
    {
        Button _btninfo = btn_info.GetComponent<Button>();
        _btninfo.onClick.AddListener(() => ShowPopupInfo());

        Button _btnShare = btn_share.GetComponent<Button>();
        _btnShare.onClick.AddListener(() => ShowPopupShare());

        Button _btnGift = btn_gift.GetComponent<Button>();
        _btnGift.onClick.AddListener(() => ShowGift());

        Button _btnSetting = btn_setting.GetComponent<Button>();
        _btnSetting.onClick.AddListener(() => GameSetting());

        Button _btnOut = btn_out.GetComponent<Button>();
        _btnOut.onClick.AddListener(() => OutGame());

    }

    private void ShowPopupInfo()
    {

    }

    private void ShowPopupShare()
    {
        PopupShare.SetActive(true);
        Button _btnShare = btn_share1.GetComponent<Button>();
        _btnShare.onClick.AddListener(() => StartCoroutine(ShareSocial()));
    }

    private void ShowGift()
    {
        HandleGiftStore();
    }
    private void GameSetting()
    {

    }

    private void OutGame()
    {
        ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.closeGame, null, APIs.token, (result) =>
        {
            try
            {
                JObject resultObj = JObject.Parse(result);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Error: " + e.Message);
            }
        });
        GetComponent<UnityMessageManager>().SendMessageToFlutter("quit");
    }
    public IEnumerator ShareSocial()
    {
        yield return new WaitForEndOfFrame();
        string filePath = Path.Combine(Application.temporaryCachePath, "sharedImg.png");
        if (File.Exists(filePath))
        {
            UnityNativeSharingHelper.ShareScreenshotAndText("", filePath, false, "Select App To Share With");
        }
        else
        {
            File.WriteAllBytes(filePath, shareImage.EncodeToPNG());

            while (!File.Exists(filePath))
                yield return new WaitForSecondsRealtime(0.05f);

            UnityNativeSharingHelper.ShareScreenshotAndText("", filePath, false, "Select App To Share With");
        }
        ApiHelper.Instance.RequestWebService(APIMethod.POST.ToString(), Endpoints.shareSocial, null, APIs.token, (result) =>
        {
            JObject resultObj = JObject.Parse(result);
            Debug.Log("result: " + result);
            string message = resultObj["message"].ToString();
            if (!string.IsNullOrEmpty(message) && message.ToLower().Contains("thành công"))
            {
                InitGame();
            }
        });

    }

    private void HandleGiftStore()
    {

    }


    private void OnApplicationQuit()
    {
        ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.closeGame, null, APIs.token, (result) =>
        {
            try
            {
                JObject resultObj = JObject.Parse(result);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Error: " + e.Message);
            }
        });
    }
}
