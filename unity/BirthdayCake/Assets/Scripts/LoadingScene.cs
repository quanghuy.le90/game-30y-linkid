using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using FlutterUnityIntegration;
using BirthdayCake.APIs.RequestModels;

public class LoadingScene : MonoBehaviour
{

    [SerializeField]
    private Slider downloadBar;
    [SerializeField]
    private Text messageText;
    [SerializeField]
    private Text remindText;
    [SerializeField]
    private GameObject quitBtn;
    [SerializeField]
    private GameObject errorDialog;
    private DateTime startDate;
    private void Start()
    {
#if UNITY_EDITOR
        GetToken("VJ/P87RQu2/99ayEB94ogIYwUYVA0jHeQE90d31m7IN2b1uaGNUDXorAivJ1o6Rnu6Vmyk+5kks63ux0MUkeMUJf1iStoqQEPZnNTpAgdLU61Z3tjj3OALTX");
#endif
    }

    public void GetToken(string message)
    {
        quitBtn.SetActive(false);
        APIs.SetToken(message);
        GetComponent<UnityMessageManager>().SendMessageToFlutter("unityloaded");

        downloadBar.value = 0;
        InitGame();
    }

    private void InitGame()
    {
        ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.initGameAndCheckIn, null, APIs.token, (result) =>
            {
                try
                {
                    JObject resultObj = JObject.Parse(result);
                    if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
                    {
                        GameInfo gameInfo = JsonConvert.DeserializeObject<GameInfo>(resultObj["result"]["gameInfo"].ToString());
                        InitGameResponse initGameResponse = JsonConvert.DeserializeObject<InitGameResponse>(resultObj["result"]["userInfo"].ToString());

                        DateTime today = DateTime.Now;
                        //DateTime today = DateTime.Parse("8/7/2023");
                        startDate = Common.ParseDateTimeFromString(gameInfo.startDate);
                        DateTime endDate = Common.ParseDateTimeFromString(gameInfo.endDate);

                        if (Common.CompareTwoDates(startDate, today) > 0)
                        {
                            //Game chưa start
                            downloadBar.gameObject.SetActive(false);
                            messageText.gameObject.SetActive(true);

                            TimeSpan diff = startDate - today;
                            string diffMessage = "";

                            if (diff.Days >= 1)
                            {
                                diffMessage = string.Format("Game chưa bắt đầu.\nQuay lại vào <color=red>{0}</color> bạn nhé", Common.DateAndHour24HFormat(startDate));
                            }
                            else
                            {
                                diffMessage = string.Format("Game chưa bắt đầu.\nQuay lại sau <color=red>{0}:{1}:{2}</color> bạn nhé!", diff.Hours.ToString("00"), diff.Minutes.ToString("00"), diff.Seconds.ToString("00"));
                                Invoke(nameof(CountTime), 1f);
                            }

                            quitBtn.SetActive(true);

                            messageText.text = diffMessage;
                        }
                        else if (Common.CompareTwoDates(today, endDate) > 0)
                        {
                            //Game đã kết thúc
                            downloadBar.gameObject.SetActive(false);
                            messageText.gameObject.SetActive(true);
                            messageText.text = "Game đã kết thúc.\nCảm ơn bạn đã tham gia.";
                            quitBtn.SetActive(true);
                        }
                        else if (Common.CompareTwoDates(startDate, today) <= 0 && Common.CompareTwoDates(today, endDate) <= 0)
                        {
                            //Game đang diễn ra
                            StartCoroutine(DownloadAssetBundle(gameInfo.version));
                            //SceneManager.LoadSceneAsync(Constant.MainScene);
                        }
                        else
                        {
                            Debug.LogError("Exception");
                        }
                        //TestShare();
                    }
                    else
                    {
                        Debug.LogError(resultObj["message"]);
                        errorDialog.SetActive(true);
                        errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                        {
                            Debug.Log("Quit");
                            errorDialog.SetActive(false);
                            GetComponent<UnityMessageManager>().SendMessageToFlutter("quit");
                        }, () =>
                        {
                            InitGame();
                            errorDialog.SetActive(false);
                        });
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError("Error result: " + result);
                    Debug.LogError("Error: " + e.Message);
                    errorDialog.SetActive(true);
                    errorDialog.GetComponent<ErrorDialog>().InitErrorDialog("Đã có lỗi xảy ra.\nVui lòng thử lại sau.", () =>
                    {
                        Debug.Log("Quit");
                        errorDialog.SetActive(false);
                        GetComponent<UnityMessageManager>().SendMessageToFlutter("quit");
                    }, () =>
                    {
                        InitGame();
                        errorDialog.SetActive(false);
                    });
                }
            });
    }

    private void CountTime()
    {
        DateTime today = DateTime.Now;
        TimeSpan diff = startDate - today;
        if (diff.Hours == 0 && diff.Minutes == 0 && diff.Seconds == 0)
        {
            CancelInvoke();
            InitGame();
            Debug.Log("Start game");
        }
        else
        {
            messageText.text = string.Format("Game chưa bắt đầu.\nQuay lại sau <color=red>{0}:{1}:{2}</color> bạn nhé!", diff.Hours.ToString("00"), diff.Minutes.ToString("00"), diff.Seconds.ToString("00"));
            Invoke(nameof(CountTime), 1f);
        }

    }

    IEnumerator DownloadAssetBundle(string gameVersion)
    {
        string url = "";
#if UNITY_ANDROID
        url = Endpoints.androidAssetBunle;
#elif UNITY_IOS
        url = Endpoints.iosAssetBunle;
#endif
        Debug.Log("gameVersion: " + gameVersion);
        if (!PlayerPrefs.HasKey(Constant.gameVersion) | PlayerPrefs.GetString(Constant.gameVersion) != gameVersion)
        {
            Caching.ClearCache();
        }

        CachedAssetBundle cachedAssetBundle = new CachedAssetBundle();
        UnityWebRequest assetBundleRequest = UnityWebRequestAssetBundle.GetAssetBundle(url, cachedAssetBundle);

        assetBundleRequest.SendWebRequest();
        while (!assetBundleRequest.isDone)
        {
            downloadBar.value = assetBundleRequest.downloadProgress;
            remindText.text = string.Format("Vui lòng chờ chút bạn nhé...{0:0.00}%", assetBundleRequest.downloadProgress*100f);
            yield return null;
        }

        switch (assetBundleRequest.result)
        {
            case UnityWebRequest.Result.InProgress:
                break;
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.ProtocolError:
            case UnityWebRequest.Result.DataProcessingError:
                Debug.LogError("Error: " + assetBundleRequest.error);
                break;
            case UnityWebRequest.Result.Success:
                downloadBar.value = 1;

                remindText.text = string.Format("Vui lòng chờ chút bạn nhé...{0}%", 100f);
                PlayerPrefs.SetString(Constant.gameVersion, gameVersion);
                AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(assetBundleRequest);
                SceneManager.LoadSceneAsync(Constant.MainScene);
                break;
        }
    }

    public void QuitGame()
    {
        GetComponent<UnityMessageManager>().SendMessageToFlutter("quit");
    }
}
