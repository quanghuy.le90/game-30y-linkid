using UnityEngine;

public class Constant
{
    public const float heightDesign = 896f;
    public const float widthDesign = 414f;
    public const int vpbAge = 30;

    public const float cameraOrthographicSize = 10f;

    public const int delayMilisecond = 200;

    public const string dateTimeServerFormat = "yyyy-MM-ddTHH:mm:sszzz";
    public const string dateTimeFormat = "dd-MM-yyyy";

    //Scenes
    public const string LoadingScene = "Loading";
    public const string MainScene = "MainMenu";
    public const string PlayScene = "GamePlay";

    public const string currentStep = "currentStep";
    public const string checkedDate = "checkedDate";

    public const string gameVersion = "gameVersion";

    public static Color32 textColor = new Color32(68, 43, 10, 255);

    public enum TreasureType { Voucher, Gift, Money, Pieces, VPBS, Accumlate, Goodluck }

    public enum PlayerState { Idle, Walk }

    public enum ErrorType { maintenance, gameNotStart, gameOver, initGame, playGame, }

}
