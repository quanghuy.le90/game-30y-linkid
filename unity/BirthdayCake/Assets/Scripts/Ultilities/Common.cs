using System;
using System.Globalization;
using UnityEngine;
using DG.Tweening;

public static class Common
{
    static CultureInfo cultureInfo = CultureInfo.GetCultureInfo("vi-VN");
    public static void BlinkAnimate(RectTransform rectTransform, bool loop = false)
    {
        rectTransform.DOScale(1.1f, 0.5f).SetEase(Ease.OutBounce).OnComplete(() =>
        {
            rectTransform.DOScale(1.0f, 0.5f).SetEase(Ease.OutBounce).OnComplete(() => { if (loop) BlinkAnimate(rectTransform, loop); });
        });
    }

    public static DateTime ParseDateTimeFromString(string date)
    {
        return DateTime.ParseExact(date, Constant.dateTimeServerFormat, CultureInfo.InvariantCulture);
    }

    public static string DateAndHour24HFormat(DateTime dateTime)
    {
        return dateTime.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
    }

    public static string DateFormat(DateTime dateTime)
    {
        return dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
    }

    public static string Hour24HFormat(DateTime dateTime)
    {
        return dateTime.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
    }

    public static int CompareTwoDates(DateTime date1, DateTime date2)
    {
        //Return int with value less than zero meaning date1 is earlier than date2.
        //Return int with value equal Zero meaning date1 is the same as date2.
        //Return int with value greater than zero meaing date1 is later than date2.
        return DateTime.Compare(date1, date2);
    }

    public static void CopyToClipboard(string code)
    {
        GUIUtility.systemCopyBuffer = code;
    }

    public static string VNDFormat(string money)
    {
        //double value = double.Parse(money.Replace(",", ""));
        //string formattedString = value.ToString("C0", cultureInfo);
        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

        nfi.CurrencyGroupSeparator = ".";
        nfi.CurrencySymbol = "";
        string formattedString = Convert.ToDecimal(money).ToString("C0",
              nfi);
        return formattedString + "đ";
    }

    public static string CheckNullString(string inputString, string nullString)
    {
        return String.IsNullOrEmpty(inputString) ? nullString : inputString;
    }
}