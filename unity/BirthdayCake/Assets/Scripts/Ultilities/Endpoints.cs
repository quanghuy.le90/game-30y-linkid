class Endpoints
{
    public const string androidAssetBunle = "https://firebasestorage.googleapis.com/v0/b/cams-b5978.appspot.com/o/live_android_30y_linkid_bundle?alt=media&token=21b4c006-0ad5-42c8-92d5-f98fbd91f364";
    public const string iosAssetBunle = "https://firebasestorage.googleapis.com/v0/b/cams-b5978.appspot.com/o/live_ios_30y_linkid_bundle?alt=media&token=c8ad0c57-adfb-47a1-a2b6-b131545581a7";
    public const string baseURL = "https://vpb30-linkid.gami.linkid.vn/";
    //public const string baseURL = "http://k8s-gamelink-gamelink-a94bf3d872-1704347399.ap-southeast-1.elb.amazonaws.com/";
    public const string initGame = "game-linkid30-be/api/v1/game-play/init";
    public const string initGameAndCheckIn = "game-linkid30-be/api/v1/game-play/init?isBegin=true";
    public const string giftUser = "game-linkid30-be/api/v1/game-play/user-gifts";
    public const string playGame = "game-linkid30-be/api/v1/game-play/play";
    public const string closeGame = "game-linkid30-be/api/v1/game-play/close-game";
    public const string giftTranfer = "game-linkid30-be/api/v1/game-play/gift-transfer";
    public const string shareSocial = "game-linkid30-be/api/v1/game-play/share";
}