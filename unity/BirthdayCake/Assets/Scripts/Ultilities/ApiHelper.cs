using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

public class ApiHelper : MonoBehaviour
{
    public static ApiHelper Instance { get; private set; }
    // Initialize the singleton instance.
    private void Awake()
    {
        Screen.fullScreen = false;
        // If there is not already an instance of GameManager, set it to this.
        if (Instance == null)
        {
            Instance = this;
        }
        //If an instance already exists, destroy whatever this object is to enforce the singleton.
        else if (Instance != this)
        {
            DestroyImmediate(gameObject);
        }
        //Set GameManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(this.gameObject);
    }

    public void RequestWebService(string method, string url, object body, string bearerToken, Action<string> callback)
    {
        StartCoroutine(RequestWebServiceCoroutine(method, url, body, bearerToken, (callbackOnFinish) =>
       {
           callback(callbackOnFinish);
       }));
    }

    IEnumerator RequestWebServiceCoroutine(string method, string url, object body, string bearerToken, Action<string> callbackOnFinish)
    {
        string fullURL = Endpoints.baseURL + url;
        // string fullURL = url.Equals(Endpoints.addPlayTurn) ? Endpoints.baseURL + url + GameManager.Instance.TURN : Endpoints.baseURL + url + GameManager.Instance.AES;

        UnityWebRequest webRequest = new UnityWebRequest();

        webRequest.url = fullURL;
        webRequest.method = method;
        webRequest.downloadHandler = new DownloadHandlerBuffer();
        webRequest.uploadHandler = new UploadHandlerRaw(string.IsNullOrEmpty(JsonUtility.ToJson(body)) ? null : Encoding.UTF8.GetBytes(body.ToString()));
        webRequest.timeout = 30;
        
        //Debug.Log("---------------- URL ----------------");

        //Debug.Log("---------------- Header ----------------");
        webRequest.SetRequestHeader("Accept", "application/json");

        webRequest.SetRequestHeader("Content-Type", "application/json; charset=UTF-8");

        if (bearerToken != null)
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + bearerToken);
        }

        yield return webRequest.SendWebRequest();


        switch (webRequest.result)
        {
            case UnityWebRequest.Result.InProgress:
                //Debug.Log("Loading");
                break;
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
            case UnityWebRequest.Result.ProtocolError:
                //Debug.Log("---------------- ERROR ----------------");
                // BaseResponse baseResponse = new BaseResponse();
                // baseResponse.message = "Mất kết nối mạng. Kiểm tra lại kết nối và tiếp tục chơi nhé!";
                //string message = "{"result": [{ "name": "Evoucher giảm 17 % dịch vụ gội đầu dưỡng sinh","eGiftCode": "MMCK8XRV","image": "https://linkidstorage.s3-ap-southeast-1.amazonaws.com/upload-gift/3024a0a818f2031138d99e12a6761de6.png" } ],"message": "Success"}";
                callbackOnFinish(webRequest.downloadHandler.text.ToString());
                break;
            case UnityWebRequest.Result.Success:
                //Debug.Log("---------------- Response Raw ----------------");
                // BaseResponse response = JsonConvert.DeserializeObject<BaseResponse>(webRequest.downloadHandler.text);
                callbackOnFinish(webRequest.downloadHandler.text);
                //try {
                // Debug.LogError("response: " + webRequest.downloadHandler.text);
                //} catch (Exception e) {
                //    Debug.Log("Error: " + e.ToString());
                //}

                break;
        }

    }

}


enum APIMethod { GET, POST }