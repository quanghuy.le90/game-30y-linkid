public class BaseResponse
{
    public int code { get; set; }
    public int status { get; set; }
    public string message { get; set; }
    public object data { get; set; }
}
