using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    //public Slider _musicSlider, _sfxSlider;

    private void OnEnable()
    {
        AudioManager.Instance.SetupAudoManager();
        //AudioManager.Instance.sfxSounds
        //AudioManager.Instance.musicSounds
    }

    public void ToggleMucsic()
    {
        AudioManager.Instance.ToggleMusic();
    }

    public void ToggleSFX()
    {
        AudioManager.Instance.ToggleSound();
    }

    //public void MusicVolume()
    //{
    //    AudioManager.Instance.MusicVolume(_musicSlider.value);
    //}

    //public void SFXVolume()
    //{
    //    AudioManager.Instance.SFXVolume(_sfxSlider.value);
    //}
}
