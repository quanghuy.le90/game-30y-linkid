﻿using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    [Header("Knife Count Display")]
    [SerializeField]
    private GameObject panelCandle;
    [SerializeField]
    private GameObject iconCandle;
    [SerializeField]

    public GameObject PopupRule;

    private void Update()
    {
        if (PopupRule.activeInHierarchy == true)
        {
            GetComponent<GameController>().enabled = false;
        }
        else
        {
            GetComponent<GameController>().enabled = true;
        }
    }
    private Color usedCandleIconColor;

    public void SetInitialDisplayCandleCount(int count)
    {
        Debug.Log("SetInitialDisplayCandleCount");
        for (int i = 0; i < count; i++)
        {
            Instantiate(iconCandle, panelCandle.transform);
        }
    }

    private int CandleIconIndexToChange = 0;
    public void DecrementDisplayedCandleCount()
    {
        GameController gameController = GetComponent<GameController>();
        if (!gameController.isPause)
        {
            panelCandle.transform.GetChild(CandleIconIndexToChange++).GetComponent<Image>().color = usedCandleIconColor;
        }
    }
}

