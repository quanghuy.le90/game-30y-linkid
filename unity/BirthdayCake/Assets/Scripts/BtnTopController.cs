using FlutterUnityIntegration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BtnTopController : MonoBehaviour
{
    [SerializeField] Button btnPause, btnReGame, btnOut;
    [SerializeField] GameObject PopPause;
    public GameController gameController;

    public void Start()
    {
        btnPause.onClick.AddListener(() => PauseGame()); ;

    }
    public void PauseGame()
    {
        //Time.timeScale = 0;
        PopPause.SetActive(true);

        Debug.LogError("pause = " + Time.timeScale);
        //btnReGame.onClick.AddListener(() =>
        //{
        //    Debug.Log("gameController: ");
        //    //gameController.PauseGame();
        //    //PopPause.SetActive(false);
        //});
        //btnOut.onClick.AddListener(() =>
        //{
        //    SceneManager.LoadScene("MainMenu");
        //});

    }
}
