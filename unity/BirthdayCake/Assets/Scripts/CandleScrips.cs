
using UnityEngine;

public class CandleScrips : MonoBehaviour
{
    public GameController gameController = null;
    [SerializeField]
    private Vector2 throwForce;

    private bool isActive = true;
    private Rigidbody2D rb;

    private BoxCollider2D candleCollider;

    public void InitCandle(GameController game)
    {
        gameController = game;
    }

    private void ReadyCandle()
    {
        readyToPlay = true;
        Debug.Log("ReadyCandle: " + readyToPlay);
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        candleCollider = GetComponent<BoxCollider2D>();
    }

    private bool readyToPlay = false;
    private bool ruleDialogDisabled = false;

    public void Update()
    {
        if (!gameController.PopupRule.activeInHierarchy && !ruleDialogDisabled)
        {
            ruleDialogDisabled = true;
            Invoke(nameof(ReadyCandle), 0.05f);
        }

        if (Input.touchCount > 0 && !gameController.isPause && readyToPlay)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.position.y < 2300)
            {
                if (touch.phase == TouchPhase.Ended && isActive)
                {
                    rb.AddForce(throwForce, ForceMode2D.Impulse);
                    rb.gravityScale = 1;
                    GameController.Instance.GameUI.DecrementDisplayedCandleCount();
                    AudioManager.Instance.PlaySFX("Hit");
                }
            }

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isActive)
        {
            return;
        }
        isActive = false;

        if (collision.collider.tag == "Log")
        {
            GetComponent<ParticleSystem>().Play();

            rb.velocity = new Vector2(0, 0);
            rb.bodyType = RigidbodyType2D.Kinematic;
            this.transform.SetParent(collision.collider.transform);

            candleCollider.offset = new Vector2(candleCollider.offset.x, -0.4f);
            candleCollider.size = new Vector2(candleCollider.size.x, 1.2f);

            GameController.Instance.OnSuccesfulCandleHit();
        }
        else if (collision.collider.tag == "Candle" || collision.collider.tag == "Kiwi")
        {
            //  FindObjectOfType<GameManager>().EndGame();
            rb.velocity = new Vector2(rb.velocity.x, -2);
            GameController.Instance.StartGameOverSequence(false);
        }
    }
}
