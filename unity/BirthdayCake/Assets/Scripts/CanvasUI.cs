using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasUI : MonoBehaviour
{
    [SerializeField] Button btnStartGame;
    [SerializeField] Text textNumberOfPlay;
    [SerializeField] GameObject Popup;
    private float numberOfPlay;

    void Start()
    {
        Button _btnStartGame = btnStartGame.GetComponent<Button>();
        _btnStartGame.onClick.AddListener(StartGame);

        void StartGame()
        {
            CheckNumberOfPlay();
        }

        Debug.Log("userID - bundleId - " +DateTime.Now);
        if (texture2D == null)
        {
            Debug.Log("Load share image");
            texture2D = Resources.Load<Texture2D>("Images/share_image");
            _btnStartGame.onClick.AddListener(OnClickShareButton);
        }
    }
    private void Awake()
    {
        numberOfPlay = UnityEngine.Random.Range(0, 3);
        textNumberOfPlay.text = ("number of plays remaining : " + numberOfPlay);
    }
    private void CheckResult()
    {
        float x = UnityEngine.Random.Range(0, 1);
        if (x == 0)
        {
            Debug.Log("result = win");
            CheckLevel();
        }
        else
        {
            Debug.Log("result = lose");
        }
    }
    private void CheckNumberOfPlay()
    {
        if (numberOfPlay > 0)
        {
            CheckResult();
        }
        else
        {
            Popup.SetActive(true);
        }
    }

    private void CheckLevel()
    {
        //  float level = Random.Range(1, 3);
        float level = 1;
        if (level == 1) { SceneManager.LoadScene("lv1"); }
        else if (level == 2) { SceneManager.LoadScene("lv2"); }
        else { SceneManager.LoadScene("lv3"); }
    }

    [SerializeField]
    private Texture2D texture2D;

    public void OnClickShareButton()
    {
        //shareMessage = "Suprise Madagasca";
        StartCoroutine(ShareSocial());
    }

    private IEnumerator ShareSocial()
    {
        yield return new WaitForEndOfFrame();

        // new NativeShare().AddFile(texture2D).SetSubject("Tap share button").SetText(shareMessage).SetCallback((result, shareTarget) =>
        // {
        //     Debug.Log("Callback Share result: " + result + name + ", selected app: " + shareTarget);
        // }).Share();
    }
}