using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorDialog : MonoBehaviour
{
    private string defaultMessage = "Đang có sự cố.\nVui lòng quay lại sau bạn nhé!";
    [SerializeField]
    private Text messageText = null;
    [SerializeField]
    private GameObject restartBtn = null;

    public delegate void CloseError();
    public delegate void RetryError();

    private CloseError closeErrorCallback;
    private RetryError retryErrorCallback;

    public void InitErrorDialog(string message, CloseError closeCallback = null, RetryError retryCallback = null)
    {
        if (!string.IsNullOrEmpty(message))
        {
            messageText.text = message;
        }
        else
        {
            messageText.text = defaultMessage;
        }

        closeErrorCallback = closeCallback;
        retryErrorCallback = retryCallback;

        if (retryCallback == null)
        {
            restartBtn.SetActive(false);
        }
        else
        {
            restartBtn.SetActive(true);
        }
    }

    public void Close()
    {
        if (closeErrorCallback != null)
        {
            closeErrorCallback();
            //Destroy(this.gameObject);

        }

    }

    public void Retry()
    {
        if (retryErrorCallback != null)
        {
            retryErrorCallback();
            //Destroy(this.gameObject);
        }
    }
}
