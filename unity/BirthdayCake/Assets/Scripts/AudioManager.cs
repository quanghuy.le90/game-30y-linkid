using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [SerializeField]
    public Sounds[] musicSounds, sfxSounds;
    [SerializeField] AudioSource musicSource, sfxSource;
    [SerializeField] GameObject imgToggleMusicOn, imgToggleMusicOff, imgToggleSFXOn, imgToggleSFXOff;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        PlayMusic("Background Music");
    }
    public void PlayMusic(string name)
    {
        Sounds s = Array.Find(musicSounds, x => x.name == name);
        if (s == null)
        {
            Debug.Log("Sound not found");
        }
        else
        {
            musicSource.clip = s.clip;
            musicSource.Play();
        }
    }

    public void PlaySFX(string name)
    {
        Sounds s = Array.Find(sfxSounds, x => x.name == name);
        if (s == null)
        {
            Debug.Log("Sound not found");
        }
        else
        {
            sfxSource.PlayOneShot(s.clip);
        }
    }

    public void ToggleMusic()
    {
        Debug.Log("ToggleMusic");
        if (musicSource.volume == 0)
        {
            musicSource.volume = 1;
            imgToggleMusicOn.SetActive(true);
            imgToggleMusicOff.SetActive(false);
        }
        else
        {
            musicSource.volume = 0;
            imgToggleMusicOn.SetActive(false);
            imgToggleMusicOff.SetActive(true);
        }
    }
    public void ToggleSound()
    {
        Debug.Log("ToggleSound");
        if (sfxSource.volume == 0)
        {
            sfxSource.volume = 1;
            imgToggleSFXOn.SetActive(true);
            imgToggleSFXOff.SetActive(false);
        }
        else
        {
            sfxSource.volume = 0;
            imgToggleSFXOn.SetActive(false);
            imgToggleSFXOff.SetActive(true);
        }
    }
    public void MusicVolume(float volume)
    {
        musicSource.volume = volume;
    }
    public void SFXVolume(float volume)
    {
        sfxSource.volume = volume;
    }

    public void SetupAudoManager()
    {

        GameObject music = GameObject.Find("MusicCheckmark");
        GameObject sound = GameObject.Find("SoundCheckmark");

        imgToggleSFXOn = music.transform.GetChild(0).gameObject;
        imgToggleSFXOff = music.transform.GetChild(1).gameObject;

        imgToggleMusicOn = sound.transform.GetChild(0).gameObject;
        imgToggleMusicOff = sound.transform.GetChild(1).gameObject;
    }

}
