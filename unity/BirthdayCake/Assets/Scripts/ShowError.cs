
using FlutterUnityIntegration;
using UnityEngine;
using UnityEngine.UI;

public class ShowError : MonoBehaviour
{
    public GameObject Popup;
    public Button closeApp;

    [SerializeField] private Canvas lv1;
    // Start is called before the first frame update
    void Update()
    {
        TextChange();
    }

    
  
    public void TextChange()
    {

        if (Application.internetReachability == NetworkReachability.NotReachable  )
        {
            Time.timeScale = 0;
            lv1.renderMode = RenderMode.ScreenSpaceOverlay;

            Popup.SetActive(true);

            closeApp.onClick.AddListener(() => {
                GetComponent<UnityMessageManager>().SendMessageToFlutter("quit");
            });
        }
        else
        {
            Time.timeScale = 1;
            Popup.SetActive(false);
          

        }
    }
}