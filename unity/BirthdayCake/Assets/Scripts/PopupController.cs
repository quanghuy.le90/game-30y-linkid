﻿
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class PopupController : MonoBehaviour
{
    [SerializeField] private Text time, ruleTitle, lv;
    [SerializeField] GameObject PopupRule, lgcake1, lgcake2, lgcake3, errorDialog;
    [SerializeField] private Canvas lv1;
    [SerializeField] Button btnClose;
    public float timeRemaining;
    public int numberlevel;

    void Start()
    {
        CheckLevel();
        Button _btnClose = btnClose.GetComponent<Button>();
        _btnClose.onClick.AddListener(() => { lv1.renderMode = RenderMode.ScreenSpaceCamera; });

    }
    void Update()
    {
        if (timeRemaining > 0)
        {
            int x = (int)(timeRemaining);

            timeRemaining -= Time.deltaTime;
            time.text = (x + "s");
        }
        else
        {
            PopupRule.SetActive(false);
            lv1.renderMode = RenderMode.ScreenSpaceCamera;
        }

    }
    public void CheckLevel()
    {
        ApiHelper.Instance.RequestWebService(APIMethod.GET.ToString(), Endpoints.initGame, null, APIs.token, (result) =>
        {
            try
            {
                JObject resultObj = JObject.Parse(result);
                if (!string.IsNullOrEmpty(resultObj["result"].ToString()))
                {


                    GameInfo gameInfo = JsonConvert.DeserializeObject<GameInfo>(resultObj["result"]["gameInfo"].ToString());
                    InitGameResponse initGameResponse = JsonConvert.DeserializeObject<InitGameResponse>(resultObj["result"]["userInfo"].ToString());
                    numberlevel = initGameResponse.level;

                    if (numberlevel == 1)
                    {
                        lv.text = ("TẦNG 1");
                        ShowPopupRule();
                        ruleTitle.text = (" Hoàn thành thắp <b>12 cây nến</b> cho <b>tầng 1</b> để nhận quà ngay");
                        lgcake1.SetActive(true);
                        ;
                    }
                    else if (numberlevel == 2)
                    {
                        lv.text = ("TẦNG 2");
                        ShowPopupRule();
                        ruleTitle.text = (" Hoàn thành thắp <b>9 cây nến</b> cho <b>tầng 2</b> để nhận quà ngay");
                        lgcake2.SetActive(true);

                    }
                    else
                    {
                        lv.text = ("TẦNG 3");
                        ShowPopupRule();
                        ruleTitle.text = (" Hoàn thành thắp <b>6 cây nến</b> cho <b>tầng 3</b> để nhận quà ngay");
                        lgcake3.SetActive(true);


                    }

                }
                else
                {
                    Debug.LogError(resultObj["message"]);
                    errorDialog.SetActive(true);
                    errorDialog.GetComponent<ErrorDialog>().InitErrorDialog(resultObj["message"].ToString(), () =>
                    {
                        Debug.Log("Quit");
                        errorDialog.SetActive(false);
                    }, () =>
                    {
                        CheckLevel();
                        errorDialog.SetActive(false);
                    });

                }
            }
            catch (System.Exception e)
            {
                Debug.LogError("Error result: " + result);
                Debug.LogError("Error: " + e.Message);
            }
        });

    }

    public void ShowPopupRule()
    {
        PopupRule.SetActive(true);
    }
}


