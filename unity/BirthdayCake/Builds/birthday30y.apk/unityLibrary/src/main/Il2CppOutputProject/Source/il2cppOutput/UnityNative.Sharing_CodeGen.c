﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_m304586EFEA7B6E54AB9082DCB87E636036E08356 (void);
extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_m0E4D222DE3B9405219CA3A7CD54E635148EDF3B4 (void);
extern void AndroidUnityNativeSharingAdapter__ctor_m02105AE1B7A9F5A637D6D49F28E8BDB56700546F (void);
extern void AndroidUnityNativeSharingAdapter_ShareScreenshotAndText_m7861490C654A66C3AFC1133D57D98D6127F16C93 (void);
extern void AndroidUnityNativeSharingAdapter_ShareText_m89EF46D182F10FB6D0BF41A31CCC92424318D6ED (void);
extern void NullUnityNativeSharingAdapter_ShareScreenshotAndText_mBFF07BA02CA3ABC0B795B17A85BAC0D45CEF545A (void);
extern void NullUnityNativeSharingAdapter_ShareText_m49C6E7AAABF7EA838A62F1BDAEC56D5189A6A45A (void);
extern void NullUnityNativeSharingAdapter__ctor_mAC6965C4A0C2239498B277EF6021CEEDE01A05B6 (void);
extern void UnityNativeSharing_Create_m659A1E5822976EFACAC96EA3C864748ACE462E13 (void);
extern void UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD (void);
extern void UnityNativeSharing_ShareScreenshotAndText_m4D796344268E8048062152322841F90EFBE213CD (void);
extern void UnityNativeSharing_ShareText_m6BE537F2B56982778AC3B7ECA28CA488D64341A9 (void);
static Il2CppMethodPointer s_methodPointers[16] = 
{
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_m304586EFEA7B6E54AB9082DCB87E636036E08356,
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_m0E4D222DE3B9405219CA3A7CD54E635148EDF3B4,
	AndroidUnityNativeSharingAdapter__ctor_m02105AE1B7A9F5A637D6D49F28E8BDB56700546F,
	AndroidUnityNativeSharingAdapter_ShareScreenshotAndText_m7861490C654A66C3AFC1133D57D98D6127F16C93,
	AndroidUnityNativeSharingAdapter_ShareText_m89EF46D182F10FB6D0BF41A31CCC92424318D6ED,
	NULL,
	NULL,
	NullUnityNativeSharingAdapter_ShareScreenshotAndText_mBFF07BA02CA3ABC0B795B17A85BAC0D45CEF545A,
	NullUnityNativeSharingAdapter_ShareText_m49C6E7AAABF7EA838A62F1BDAEC56D5189A6A45A,
	NullUnityNativeSharingAdapter__ctor_mAC6965C4A0C2239498B277EF6021CEEDE01A05B6,
	NULL,
	NULL,
	UnityNativeSharing_Create_m659A1E5822976EFACAC96EA3C864748ACE462E13,
	UnityNativeSharing__ctor_mB87CF76AD4101E18E0FE4B2426787E85B16F86BD,
	UnityNativeSharing_ShareScreenshotAndText_m4D796344268E8048062152322841F90EFBE213CD,
	UnityNativeSharing_ShareText_m6BE537F2B56982778AC3B7ECA28CA488D64341A9,
};
static const int32_t s_InvokerIndices[16] = 
{
	4577,
	2255,
	2255,
	414,
	983,
	0,
	0,
	414,
	983,
	2255,
	0,
	0,
	4528,
	2012,
	414,
	983,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityNative_Sharing_CodeGenModule;
const Il2CppCodeGenModule g_UnityNative_Sharing_CodeGenModule = 
{
	"UnityNative.Sharing.dll",
	16,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
